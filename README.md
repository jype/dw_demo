# DW Demo

A PSX Digimon World map viewer, animation viewer and background music player.

![Map Viewer](https://i.imgur.com/oaw5rk4.png)
![Animation Viewer](https://i.imgur.com/oUfM89o.png)
![Background Music Player](https://i.imgur.com/OLqny65.png)

## Dependencies

Install the following packages:
```
cmake gcc g++ libgmp-dev libmpfr-dev libmpc-dev texinfo
```

### Build binutils

```
wget https://ftp.gnu.org/gnu/binutils/binutils-2.42.tar.xz
tar -xf binutils-2.42.tar.xz
mkdir -p binutils-2.42-build
cd binutils-2.42-build
../binutils-2.42/configure --prefix=/usr/local/mipsel-unknown-elf \
--target=mipsel-unknown-elf --with-float=soft

make -j$(nproc)
# May require root privileges
make install

# Add binutils install directory to PATH variable
```

### Build GCC

```
wget https://ftp.gnu.org/gnu/gcc/gcc-13.2.0/gcc-13.2.0.tar.xz
tar -xf gcc-13.2.0.tar.xz
mkdir -p gcc-13.2.0-build
cd gcc-13.2.0-build
../gcc-13.2.0/configure --prefix=/usr/local/mipsel-unknown-elf \
--target=mipsel-unknown-elf --enable-languages=c,c++ --disable-lns \
--disable-libquadmath --disable-libada --disable-libssp \
--disable-libstdcxx --with-float=soft --with-gnu-as --with-gnu-ld

make -j$(nproc)
# May require root privileges
make install

# Add GCC install directory to PATH variable
```

### Build mkpsxiso

```
git clone https://github.com/Lameguy64/mkpsxiso.git
cd mkpsxiso
git submodule update --init --recursive
cmake .
make -j$(nproc)
# May require root privileges
make install
```

### Psy-Q headers and libraries

Download converted Psy-Q headers and libraries or convert them from the Psy-Q SDK with https://gitlab.com/jype/psyq2elf or https://github.com/grumpycoders/pcsx-redux/tree/main/tools/psyq-obj-parser.

## Configure

Create file `local.mk` and add the following configuration:
```
PSYQ=/path/to/converted/psyq
```

## Build

```
# Dump original PSX Digimon World (USA) ISO
dumpsxiso -x /path/to/extracted/iso "/path/to/Digimon World (USA).bin"

# Extract data and copy media
./build-data.sh /path/to/extracted/iso
./build-media.sh /path/to/extracted/iso

# Build executable and CD image
make -j$(nproc)
```

## Links

Map, model and animation code is based on work by SydMontague:  
https://github.com/SydMontague/DW1-Code  
https://github.com/Operation-Decoded/DW1ModelConverter

Font storage format is from DW1 font editor by uzuhenry:  
https://github.com/uzuhenry/DW1--font-editor

inline_n.h is from PCSX-Redux nugget:  
https://github.com/pcsx-redux/nugget
