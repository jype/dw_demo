# See LICENSE file for copyright and license details.

.EXTRA_PREREQS := $(abspath $(lastword $(MAKEFILE_LIST)))

-include local.mk

TOOLCHAIN ?= mipsel-unknown-elf-

# PSYQ := <path to PSYQ SDK>

ifeq ($(PSYQ),)
$(error PSYQ SDK path is not set)
endif

BUILDDIR ?= build

ELF := $(BUILDDIR)/dw_demo
EXE := $(BUILDDIR)/dw_demo.exe
BIN := $(BUILDDIR)/dw_demo.bin
CUE := $(BUILDDIR)/dw_demo.cue

CC := $(TOOLCHAIN)gcc
LD := $(TOOLCHAIN)ld
OBJCOPY := $(TOOLCHAIN)objcopy

MKPSXISO ?= mkpsxiso

INC := -I$(PSYQ)/include -Iinclude

LIB := \
	$(PSYQ)/lib/libcard.a \
	$(PSYQ)/lib/libpress.a \
	$(PSYQ)/lib/libgs.a \
	$(PSYQ)/lib/libgpu.a \
	$(PSYQ)/lib/libgte.a \
	$(PSYQ)/lib/libds.a \
	$(PSYQ)/lib/libcd.a \
	$(PSYQ)/lib/libetc.a \
	$(PSYQ)/lib/libsn.a \
	$(PSYQ)/lib/libsnd.a \
	$(PSYQ)/lib/libspu.a \
	$(PSYQ)/lib/libmath.a \
	$(PSYQ)/lib/libcomb.a \
	$(PSYQ)/lib/libcard.a \
	$(PSYQ)/lib/libtap.a \
	$(PSYQ)/lib/libsio.a \
	$(PSYQ)/lib/libpad.a \
	$(PSYQ)/lib/libc2.a \
	$(PSYQ)/lib/libapi.a

CFLAGS := -g -Wall -Wextra -std=c99 -O3 -G0 -ffreestanding $(INC)
CPPFLAGS := -DNOSTDLIB
# CPPFLAGS := -DNOSTDLIB -DDW_DEBUG_ALLOC
DEPFLAGS = -MMD
LDFLAGS := -g -T linker.ld

SRC := \
       src/alloc.c \
       src/bg.c \
       src/bin.s \
       src/cd.c \
       src/db.c \
       src/entity.c \
       src/graphics.c \
       src/gs.c \
       src/header.c \
       src/main.c \
       src/map.c \
       src/mtn.c \
       src/params.c \
       src/pmd.c \
       src/quad.c \
       src/skeleton.c \
       src/test_map.c \
       src/test_mtn.c \
       src/tfs.c \
       src/tim.c \
       src/utils.c

OBJ := $(SRC:%=$(BUILDDIR)/%.o)
DEP := $(OBJ:%.o=%.d)

MEDIA := $(shell find $(BUILDDIR)/media -type f)

XML := dw_demo.xml

all: $(BIN)

$(ELF): $(OBJ)
	@mkdir -p $(dir $@)
	$(LD) $(LDFLAGS) -o $@ $^ $(LIB)

$(EXE): $(ELF)
	@mkdir -p $(dir $@)
	$(OBJCOPY) -O binary $< $@

$(BIN): $(XML) $(EXE) $(MEDIA)
	@mkdir -p $(dir $@)
	$(MKPSXISO) -q -y -o $(BIN) -c $(CUE) $<

-include $(DEP)

$(BUILDDIR)/%.c.o: %.c
	@mkdir -p $(dir $@)
	$(CC) -c $(CFLAGS) $(CPPFLAGS) $(DEPFLAGS) -o $@ $<

$(BUILDDIR)/%.s.o: %.s
	@mkdir -p $(dir $@)
	$(CC) -c $(CFLAGS) $(CPPFLAGS) $(DEPFLAGS) -o $@ $<

clean:
	rm -f $(ELF) $(EXE) $(BIN) $(CUE) $(OBJ) $(DEP)

.PHONY: all clean
