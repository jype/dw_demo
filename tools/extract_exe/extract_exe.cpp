#include <cassert>
#include <cstdint>

#include <array>
#include <fstream>
#include <iostream>
#include <map>

#include "bone.h"
#include "map.h"
#include "params.h"

#define PSEXE_OFFSET(offset) (((offset) - 0x90000) & 0x7fffffff)

struct exe_offsets
{
	uint32_t name_offset;
	uint32_t para_offset;
	uint32_t skel_offset;
	uint32_t map_entry_offset;
	uint32_t map_name_ptr_offset;
	uint32_t toilet_data_offset;
	uint32_t doors_data_offset;
	uint32_t font_offset;
};

static const exe_offsets slus_exe_offsets = {
	PSEXE_OFFSET(0x133b44),
	PSEXE_OFFSET(0x12ceb4),
	PSEXE_OFFSET(0x11ce60),
	PSEXE_OFFSET(0x1292d4),
	PSEXE_OFFSET(0x1291bc),
	PSEXE_OFFSET(0x122e10),
	PSEXE_OFFSET(0x122e60),
	PSEXE_OFFSET(0x130880),
};

static const exe_offsets *offsets = &slus_exe_offsets;

static std::array<dw_params, DW_NUM_CHARACTER_PARAMS> params;
static std::array<dw_map_entry, DW_NUM_MAP_ENTRIES> map_entries;

template<typename T>
static T read_sizeof(std::istream& is)
{
	T t;
	is.read(reinterpret_cast<char *>(&t), sizeof(T));
	return t;
}

template<typename T>
static void write_sizeof(std::ostream& os, const T& t)
{
	os.write(reinterpret_cast<const char *>(&t), sizeof(T));
}

static void character_model_name_extract(std::istream& is,
					 const std::string& output)
{
	std::ofstream ofs(output + "/character_model_name.bin",
			  std::ios::binary);
	ofs.exceptions(std::ifstream::badbit | std::ifstream::failbit);

	is.seekg(offsets->name_offset);
	for (size_t i = 0; i < params.size(); ++i)
	{
		char name[8];
		is.read(name, sizeof(name));
		ofs.write(name, sizeof(name));
	}

	ofs.close();
}

static void character_params_read(std::istream& is)
{
	is.seekg(offsets->para_offset);
	for (size_t i = 0; i < params.size(); ++i)
	{
		params[i] = read_sizeof<dw_params>(is);
	}
}

static void character_params_write(const std::string& output)
{
	std::ofstream ofs(output + "/character_params.bin", std::ios::binary);
	ofs.exceptions(std::ifstream::badbit | std::ifstream::failbit);

	for (size_t i = 0; i < params.size(); ++i)
	{
		write_sizeof<dw_params>(ofs, params[i]);
	}

	ofs.close();
}

static void skeleton_node_extract(std::istream& is, const std::string& output)
{
	std::ofstream node_offset(output + "/skeleton_node_offset.bin",
			       std::ios::binary);
	node_offset.exceptions(std::ifstream::badbit |
			       std::ifstream::failbit);

	std::ofstream node(output + "/skeleton_node.bin",
			       std::ios::binary);
	node.exceptions(std::ifstream::badbit | std::ifstream::failbit);

	is.seekg(offsets->skel_offset);
	for (size_t i = 0; i < params.size(); ++i)
	{
		uint32_t offset = node.tellp();
		write_sizeof<uint32_t>(node_offset, offset);

		is.seekg(offsets->skel_offset + i * sizeof(uint32_t));
		uint32_t skel_offset = read_sizeof<uint32_t>(is);
		is.seekg(PSEXE_OFFSET(skel_offset));

		for (int32_t j = 0; j < params[i].num_bones; ++j)
		{
			dw_bone bone = read_sizeof<dw_bone>(is);
			write_sizeof<dw_bone>(node, bone);
		}
	}

	node.close();
	node_offset.close();
}

static void map_entry_read(std::istream& is)
{
	is.seekg(offsets->map_entry_offset);
	for (size_t i = 0; i < map_entries.size(); ++i)
	{
		map_entries[i] = read_sizeof<dw_map_entry>(is);
	}
}

static void map_entry_write(const std::string& output)
{
	std::ofstream ofs(output + "/map_entry.bin", std::ios::binary);
	ofs.exceptions(std::ifstream::badbit | std::ifstream::failbit);

	for (size_t i = 0; i < map_entries.size(); ++i)
	{
		write_sizeof<dw_map_entry>(ofs, map_entries[i]);
	}

	ofs.close();
}

static void map_loading_name_extract(std::istream& is,
				     const std::string& output)
{
	const size_t name_len = 24;

	std::ofstream name_offset(output + "/map_loading_name_offset.bin",
				      std::ios::binary);
	name_offset.exceptions(std::ifstream::badbit |
				   std::ifstream::failbit);

	std::ofstream name(output + "/map_loading_name.bin",
			   std::ios::binary);
	name.exceptions(std::ifstream::badbit | std::ifstream::failbit);

	std::map<uint8_t, std::array<char, name_len>> names;
	uint8_t max_id = 0;

	for (size_t i = 0; i < map_entries.size(); ++i)
	{
		uint8_t id = map_entries[i].loading_name_id;
		max_id = std::max(id, max_id);

		if (names.find(id) == names.end())
		{
			is.seekg(offsets->map_name_ptr_offset +
				 id * sizeof(uint32_t));
			uint32_t offset = read_sizeof<uint32_t>(is);
			is.seekg(PSEXE_OFFSET(offset));
			is.read(names[id].data(), name_len);
		}
	}

	assert(!names.empty());

	for (uint8_t i = 0; i <= max_id; ++i)
	{
		uint32_t offset = 0;
		auto it = names.find(i);
		if (it != names.end())
		{
			offset = name.tellp();
			name.write(it->second.data(), name_len);
		}

		write_sizeof<uint32_t>(name_offset, offset);
	}

	name.close();
	name_offset.close();
}

static void map_toilet_extract(std::istream& is, const std::string& output)
{
	std::ofstream ofs(output + "/map_toilet.bin", std::ios::binary);
	ofs.exceptions(std::ifstream::badbit | std::ifstream::failbit);

	std::map<uint8_t, dw_map_toilet> toilets;
	uint8_t max_id = 0;

	for (size_t i = 0; i < map_entries.size(); ++i)
	{
		uint8_t id = map_entries[i].toilet_id;
		if (!id)
		{
			continue;
		}

		id -= 1;
		max_id = std::max(id, max_id);

		if (toilets.find(id) != toilets.end())
		{
			continue;
		}

		is.seekg(offsets->toilet_data_offset +
			 id * sizeof(dw_map_toilet));
		toilets[id] = read_sizeof<dw_map_toilet>(is);
	}

	assert(!toilets.empty());

	for (uint8_t i = 0; i <= max_id; ++i)
	{
		write_sizeof<dw_map_toilet>(ofs, toilets[i]);
	}

	ofs.close();
}

static void map_doors_extract(std::istream& is, const std::string& output)
{
	std::ofstream ofs(output + "/map_doors.bin", std::ios::binary);
	ofs.exceptions(std::ifstream::badbit | std::ifstream::failbit);

	std::map<uint8_t, dw_map_doors> doors;
	uint8_t max_id = 0;

	for (size_t i = 0; i < map_entries.size(); ++i)
	{
		uint8_t id = map_entries[i].doors_id;
		if (!id)
		{
			continue;
		}

		id -= 1;
		max_id = std::max(id, max_id);

		if (doors.find(id) != doors.end())
		{
			continue;
		}

		is.seekg(offsets->doors_data_offset + id * sizeof(dw_map_doors));
		doors[id] = read_sizeof<dw_map_doors>(is);
	}

	assert(!doors.empty());

	for (uint8_t i = 0; i <= max_id; ++i)
	{
		write_sizeof<dw_map_doors>(ofs, doors[i]);
	}

	ofs.close();
}

static void font_extract(std::istream& is, const std::string& output)
{
	std::ofstream font(output + "/font.bin", std::ios::binary);
	font.exceptions(std::ifstream::badbit | std::ifstream::failbit);

	std::ofstream font_clut(output + "/font_clut.bin", std::ios::binary);
	font_clut.exceptions(std::ifstream::badbit | std::ifstream::failbit);

	is.seekg(offsets->font_offset);

	for (uint8_t i = 0; i < 79; ++i) {
		char c[24];
		is.read(c, sizeof(c));
		font.write(c, sizeof(c));
	}

	font.close();

	char clut[16 * 2];
	is.read(clut, sizeof(clut));
	font_clut.write(clut, sizeof(clut));

	font_clut.close();
}

static void usage(const char *argv0)
{
	std::cout << "usage: " << argv0;
	std::cout << " <EXE file> <output directory>" << std::endl;
}

int main(int argc, char *argv[])
{
	const char *argv0 = argv[0];

	if (argc != 3)
	{
		usage(argv0);
		return 1;
	}

	std::ifstream ifs(argv[1], std::ios::binary);
	ifs.exceptions(std::ifstream::badbit | std::ifstream::failbit);

	std::string output(argv[2]);

	character_model_name_extract(ifs, output);
	character_params_read(ifs);
	character_params_write(output);
	skeleton_node_extract(ifs, output);
	map_entry_read(ifs);
	map_entry_write(output);
	map_loading_name_extract(ifs, output);
	map_toilet_extract(ifs, output);
	map_doors_extract(ifs, output);
	font_extract(ifs, output);

	ifs.close();

	return 0;
}
