#include <cassert>
#include <cstdint>
#include <cstring>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <vector>
#include <map>
#include <set>

template<typename T>
static T read_sizeof(std::istream& is)
{
	T t;
	is.read(reinterpret_cast<char *>(&t), sizeof(T));
	return t;
}

template<typename T>
static void write_sizeof(std::ostream& os, const T& t)
{
	os.write(reinterpret_cast<const char *>(&t), sizeof(T));
}

static void stream_copy(std::istream& is, std::ostream& os, size_t bytes)
{
	char buf[4096];

	while (bytes) {
		size_t n = std::min(bytes, sizeof(buf));
		is.read(buf, n);
		os.write(buf, n);
		bytes -= n;
	}
}

static void usage(const char *argv0)
{
	std::cout << "usage: " << argv0 << " <MMD file>" << std::endl;
}

int main(int argc, char *argv[])
{
	const char *argv0 = argv[0];

	if (argc != 2)
	{
		usage(argv0);
		return 1;
	}

	std::string mmd_path(argv[1]);

	std::string stem(mmd_path);
	size_t pos = stem.rfind('.');
	if (pos != std::string::npos) {
		stem.erase(pos);
	}

	std::string tmd_path = stem + ".TMD";
	std::string mtn_path = stem + ".MTN";

	std::ifstream mmd(mmd_path, std::ios::binary);
	mmd.exceptions(std::ifstream::badbit | std::ifstream::failbit);

	uint32_t tmd_offset = read_sizeof<uint32_t>(mmd);
	uint32_t mtn_offset = read_sizeof<uint32_t>(mmd);
	assert(tmd_offset < mtn_offset);

	size_t tmd_size = mtn_offset - tmd_offset;

	std::ofstream tmd(tmd_path, std::ios::binary);
	tmd.exceptions(std::ifstream::badbit | std::ifstream::failbit);

	mmd.seekg(tmd_offset);
	stream_copy(mmd, tmd, tmd_size);

	tmd.close();

	mmd.seekg( 0, std::ios::end);
	std::streampos mmd_end = mmd.tellg();

	mmd.seekg(mtn_offset);
	size_t mtn_size = mmd_end - mmd.tellg();

	std::ofstream mtn(mtn_path, std::ios::binary);
	mtn.exceptions(std::ifstream::badbit | std::ifstream::failbit);

	stream_copy(mmd, mtn, mtn_size);

	mtn.close();
	mmd.close();

	return 0;
}
