#include <cassert>
#include <cstdint>
#include <cstring>

#include <algorithm>
#include <fstream>
#include <iostream>
#include <vector>
#include <map>
#include <set>

#define setRGB0(p, _r0, _g0, _b0) \
	(p)->r0 = _r0, (p)->g0 = _g0, (p)->b0 = _b0

#define setRGB1(p, _r1, _g1, _b1) \
	(p)->r1 = _r1, (p)->g1 = _g1, (p)->b1 = _b1

#define setRGB2(p, _r2, _g2, _b2) \
	(p)->r2 = _r2, (p)->g2 = _g2, (p)->b2 = _b2

#define setRGB3(p, _r3, _g3, _b3) \
	(p)->r3 = _r3, (p)->g3 = _g3, (p)->b3 = _b3

#define setUV3(p, _u0, _v0, _u1, _v1, _u2, _v2)			\
	(p)->u0 = (_u0), (p)->v0 = (_v0),			\
	(p)->u1 = (_u1), (p)->v1 = (_v1),			\
	(p)->u2 = (_u2), (p)->v2 = (_v2)

#define setUV4(p, _u0, _v0, _u1, _v1, _u2, _v2, _u3, _v3)	\
	(p)->u0 = (_u0), (p)->v0 = (_v0),			\
	(p)->u1 = (_u1), (p)->v1 = (_v1),			\
	(p)->u2 = (_u2), (p)->v2 = (_v2),			\
	(p)->u3 = (_u3), (p)->v3 = (_v3)

#define setlen( p, _len)	(((P_TAG *)(p))->len  = (uint8_t)(_len))
#define setaddr(p, _addr)	(((P_TAG *)(p))->addr = (uint32_t)(_addr))
#define setcode(p, _code)	(((P_TAG *)(p))->code = (uint8_t)(_code))

#define getcode(p)		(uint8_t)(((P_TAG *)(p))->code)

#define setSemiTrans(p, abe)					\
	((abe) ?						\
	 setcode(p, getcode(p) | 0x02) :			\
	 setcode(p, getcode(p) & ~0x02))

#define setShadeTex(p, tge) \
	((tge) ?						\
	 setcode(p, getcode(p) | 0x01) :			\
	 setcode(p, getcode(p) & ~0x01))

#define setPolyF3(p)		setlen(p, 4),	setcode(p, 0x20)
#define setPolyFT3(p)		setlen(p, 7),	setcode(p, 0x24)
#define setPolyG3(p)		setlen(p, 6),	setcode(p, 0x30)
#define setPolyGT3(p)		setlen(p, 9),	setcode(p, 0x34)
#define setPolyF4(p)		setlen(p, 5),	setcode(p, 0x28)
#define setPolyFT4(p)		setlen(p, 9),	setcode(p, 0x2c)
#define setPolyG4(p)		setlen(p, 8),	setcode(p, 0x38)
#define setPolyGT4(p)		setlen(p, 12),	setcode(p, 0x3c)

typedef struct {
	uint32_t addr: 24;
	uint32_t len: 8;
	uint8_t r0, g0, b0, code;
} P_TAG;

static_assert(sizeof(P_TAG) == 8, "");

typedef struct {
	uint32_t tag;
	uint8_t r0, g0, b0, code;
	int16_t x0, y0;
	int16_t x1, y1;
	int16_t x2, y2;
} POLY_F3;

static_assert(sizeof(POLY_F3) == 20, "");

typedef struct {
	uint32_t tag;
	uint8_t r0, g0, b0, code;
	int16_t x0, y0;
	int16_t x1, y1;
	int16_t x2, y2;
	int16_t x3, y3;
} POLY_F4;

static_assert(sizeof(POLY_F4) == 24, "");

typedef struct {
	uint32_t tag;
	uint8_t r0, g0, b0, code;
	int16_t x0, y0;
	uint8_t u0, v0; int16_t clut;
	int16_t x1, y1;
	uint8_t u1, v1; int16_t tpage;
	int16_t x2, y2;
	uint8_t u2, v2; int16_t pad1;
} POLY_FT3;

static_assert(sizeof(POLY_FT3) == 32, "");

typedef struct {
	uint32_t tag;
	uint8_t r0, g0, b0, code;
	int16_t x0, y0;
	uint8_t u0, v0; int16_t clut;
	int16_t x1, y1;
	uint8_t u1, v1; int16_t tpage;
	int16_t x2, y2;
	uint8_t u2, v2; int16_t pad1;
	int16_t x3, y3;
	uint8_t u3, v3; int16_t pad2;
} POLY_FT4;

static_assert(sizeof(POLY_FT4) == 40, "");

typedef struct {
	uint32_t tag;
	uint8_t r0, g0, b0, code;
	int16_t x0, y0;
	uint8_t r1, g1, b1, pad1;
	int16_t x1, y1;
	uint8_t r2, g2, b2, pad2;
	int16_t x2, y2;
} POLY_G3;

static_assert(sizeof(POLY_G3) == 28, "");

typedef struct {
	uint32_t tag;
	uint8_t r0, g0, b0, code;
	int16_t x0, y0;
	uint8_t r1, g1, b1, pad1;
	int16_t x1, y1;
	uint8_t r2, g2, b2, pad2;
	int16_t x2, y2;
	uint8_t r3, g3, b3, pad3;
	int16_t x3, y3;
} POLY_G4;

static_assert(sizeof(POLY_G4) == 36, "");

typedef struct {
	uint32_t tag;
	uint8_t r0, g0, b0, code;
	int16_t x0, y0;
	uint8_t u0, v0; int16_t clut;
	uint8_t r1, g1, b1, p1;
	int16_t x1, y1;
	uint8_t u1, v1; int16_t tpage;
	uint8_t r2, g2, b2, p2;
	int16_t x2, y2;
	uint8_t u2, v2; int16_t pad2;
} POLY_GT3;

static_assert(sizeof(POLY_GT3) == 40, "");

typedef struct {
	uint32_t tag;
	uint8_t r0, g0, b0, code;
	int16_t x0, y0;
	uint8_t u0, v0; int16_t clut;
	uint8_t r1, g1, b1, p1;
	int16_t x1, y1;
	uint8_t u1, v1; int16_t tpage;
	uint8_t r2, g2, b2, p2;
	int16_t x2, y2;
	uint8_t u2, v2; int16_t pad2;
	uint8_t r3, g3, b3, p3;
	int16_t x3, y3;
	uint8_t u3, v3; int16_t pad3;
} POLY_GT4;

static_assert(sizeof(POLY_GT4) == 52, "");

template<typename T>
static T read_sizeof(std::istream& is)
{
	T t;
	is.read(reinterpret_cast<char *>(&t), sizeof(T));
	return t;
}

template<typename T>
static void write_sizeof(std::ostream& os, const T& t)
{
	os.write(reinterpret_cast<const char *>(&t), sizeof(T));
}

struct svector
{
	int16_t x;
	int16_t y;
	int16_t z;
	int16_t pad;
};

struct cvector
{
	uint8_t r;
	uint8_t g;
	uint8_t b;
	uint8_t pad;
};

struct uv_coord
{
	uint8_t u;
	uint8_t v;
};

struct tmd_primitive_header
{
	uint8_t olen;
	uint8_t ilen;
	uint8_t flag;
	uint8_t mode;
};

class tmd_primitive
{
public:
	tmd_primitive_header header;
	std::vector<uint16_t> vertex_indices;
	std::vector<uint16_t> normal_indices;
	std::vector<cvector> colors;
	std::vector<uv_coord> uvs;
	uint16_t clut;
	uint16_t tpage;

	tmd_primitive(std::istream& is);

	bool is_light_source_disabled() const;
	bool is_double_faced() const;
	bool is_gradated() const;

	bool has_brightness() const;
	bool has_translucency() const;
	bool has_texture() const;
	bool is_quad() const;
	bool is_gouraud() const;
	uint8_t option() const;
};

tmd_primitive::tmd_primitive(std::istream& is)
{
	header = read_sizeof<tmd_primitive_header>(is);
	assert(option() == 1);

	size_t num_vertices = 3;
	if (is_quad())
	{
		num_vertices = 4;
	}

	size_t num_normals = 1;
	if (is_light_source_disabled() || has_brightness())
	{
		num_normals = 0;
	}
	else if (is_gouraud())
	{
		num_normals = num_vertices;
	}

	size_t num_colors = 0;
	if (is_gradated())
	{
		num_colors = num_vertices;
	}
	else if (is_light_source_disabled())
	{
		num_colors = 1;
		if (is_gouraud())
		{
			num_colors = num_vertices;
		}
	}
	else if (!has_texture())
	{
		num_colors = 1;
	}

	if (has_texture())
	{
		uvs.emplace_back(read_sizeof<uv_coord>(is));
		clut = read_sizeof<uint16_t>(is);
		uvs.emplace_back(read_sizeof<uv_coord>(is));
		tpage = read_sizeof<uint16_t>(is);
		uvs.emplace_back(read_sizeof<uv_coord>(is));
		is.seekg(2, std::ios::ios_base::cur);
		if (is_quad())
		{
			uvs.emplace_back(read_sizeof<uv_coord>(is));
			is.seekg(2, std::ios::ios_base::cur);
		}
	}

	for (uint32_t i = 0; i < num_colors; ++i)
	{
		colors.emplace_back(read_sizeof<cvector>(is));
	}

	for (uint32_t i = 0; i < num_vertices; ++i)
	{
		if (i < num_normals)
		{
			normal_indices.emplace_back(read_sizeof<uint16_t>(is));
		}

		vertex_indices.emplace_back(read_sizeof<uint16_t>(is));
	}
}

bool tmd_primitive::is_light_source_disabled() const
{
	return !!(header.flag & (1 << 0));
}

bool tmd_primitive::is_double_faced() const
{
	return !!(header.flag & (1 << 1));
}

bool tmd_primitive::is_gradated() const
{
	return !!(header.flag & (1 << 2));
}

bool tmd_primitive::has_brightness() const
{
	return !!(header.mode & (1 << 0));
}

bool tmd_primitive::has_translucency() const
{
	return !!(header.mode & (1 << 1));
}

bool tmd_primitive::has_texture() const
{
	return !!(header.mode & (1 << 2));
}

bool tmd_primitive::is_quad() const
{
	return !!(header.mode & (1 << 3));
}

bool tmd_primitive::is_gouraud() const
{
	return !!(header.mode & (1 << 4));
}

uint8_t tmd_primitive::option() const
{
	return (header.mode >> 5) & 0x7;
}

struct tmd_object_header
{
	uint32_t vert_top;
	uint32_t n_vert;
	uint32_t normal_top;
	uint32_t n_normal;
	uint32_t primitive_top;
	uint32_t n_primitive;
	int32_t scale;
};

struct tmd_object
{
	tmd_object_header header;
	std::vector<svector> vertices;
	std::vector<svector> normals;
	std::vector<tmd_primitive> primitives;

	tmd_object(std::istream& is, std::istream::pos_type obj_offset);
};

tmd_object::tmd_object(std::istream& is, std::istream::pos_type obj_offset)
{
	using pos_type = std::istream::pos_type;
	using off_type = std::istream::off_type;

	header = read_sizeof<tmd_object_header>(is);

	auto vert_top = obj_offset + (pos_type)header.vert_top;
	is.seekg(vert_top);
	for (uint32_t i = 0; i < header.n_vert; ++i)
	{
		vertices.emplace_back(read_sizeof<svector>(is));
	}

	auto normal_top = obj_offset + (pos_type)header.normal_top;
	is.seekg(normal_top);
	for (uint32_t i = 0; i < header.n_normal; ++i)
	{
		normals.emplace_back(read_sizeof<svector>(is));
	}

	auto primitive_top = (obj_offset + (pos_type)header.primitive_top);
	is.seekg(primitive_top);
	for (uint32_t i = 0; i < header.n_primitive; ++i)
	{
		primitives.emplace_back(is);

		off_type offset = is.tellg();
		is.seekg((4 - (offset - primitive_top) % 4) % 4,
			 std::ios_base::cur);
	}
}

struct tmd_model_header
{
	uint32_t id;
	uint32_t flags;
	uint32_t nobj;
};

class tmd_model
{
public:
	tmd_model_header header;
	std::vector<tmd_object> objects;

	tmd_model(std::istream& is);

	void to_pmd(std::ostream& is, bool shared) const;
};

tmd_model::tmd_model(std::istream& is)
{
	using pos_type = std::istream::pos_type;

	header = read_sizeof<tmd_model_header>(is);

	std::istream::pos_type obj_offset = is.tellg();

	for (uint32_t i = 0; i < header.nobj; ++i)
	{
		pos_type offset = (obj_offset +
				   (pos_type)(i * sizeof(tmd_object_header)));
		is.seekg(offset);
		objects.emplace_back(is, obj_offset);
	}
}

class pmd_primitive
{
public:
	uint16_t type;
	std::vector<uint16_t> vertex_indices;
	std::vector<uint16_t> normal_indices;
	std::vector<cvector> colors;
	std::vector<uv_coord> uvs;
	uint16_t clut;
	uint16_t tpage;
	bool has_brightness;
	bool has_translucency;

	pmd_primitive(const tmd_primitive& primitive, bool shared);

	friend std::ostream& operator<<(std::ostream& os,
					const pmd_primitive& primitive);
};

pmd_primitive::pmd_primitive(const tmd_primitive& primitive, bool shared)
	: type(0),
	  vertex_indices(primitive.vertex_indices),
	  normal_indices(primitive.normal_indices),
	  colors(primitive.colors),
	  uvs(primitive.uvs),
	  clut(primitive.clut),
	  tpage(primitive.tpage),
	  has_brightness(primitive.has_brightness()),
	  has_translucency(primitive.has_translucency())
{
	assert(!primitive.is_double_faced());
	assert(!primitive.is_gradated());

	type |= (primitive.is_quad() ? 1 : 0) << 0;
	type |= (primitive.is_gouraud() ? 1 : 0) << 1;
	type |= (primitive.has_texture() ? 0 : 1) << 2;
	type |= (shared ? 1 : 0) << 3;
	type |= (primitive.is_light_source_disabled() ? 0 : 1) << 4;
	type |= (primitive.is_double_faced() ? 1 : 0) << 5;
}

std::ostream& operator<<(std::ostream& os, const pmd_primitive& primitive)
{
	cvector colors[4] = {
		{ 0x80, 0x80, 0x80, 0 },
		{ 0x80, 0x80, 0x80, 0 },
		{ 0x80, 0x80, 0x80, 0 },
		{ 0x80, 0x80, 0x80, 0 },
	};
	if (primitive.colors.size() <= 1)
	{
		cvector color = { 0x80, 0x80, 0x80, 0 };
		if (primitive.colors.size() == 1)
		{
			color = primitive.colors[0];
		}

		for (size_t i = 0; i < primitive.colors.size(); ++i)
		{
			colors[i] = color;
		}
	}
	else
	{
		for (size_t i = 0; i < primitive.colors.size(); ++i)
		{
			colors[i] = primitive.colors[i];
		}
	}

	switch (primitive.type & 0x7)
	{
	case 0:
		{
			POLY_FT3 ft3;
			std::memset(&ft3, 0, sizeof(ft3));
			setPolyFT3(&ft3);
			setRGB0(&ft3, colors[0].r, colors[0].g, colors[0].b);
			assert(primitive.uvs.size() == 3);
			setUV3(&ft3,
			       primitive.uvs[0].u, primitive.uvs[0].v,
			       primitive.uvs[1].u, primitive.uvs[1].v,
			       primitive.uvs[2].u, primitive.uvs[2].v);
			ft3.clut = primitive.clut;
			ft3.tpage = primitive.tpage;
			setSemiTrans(&ft3, primitive.has_translucency);
			setShadeTex(&ft3, primitive.has_brightness);
			write_sizeof<POLY_FT3>(os, ft3);
			write_sizeof<POLY_FT3>(os, ft3);
		}
		break;
	case 1:
		{
			POLY_FT4 ft4;
			std::memset(&ft4, 0, sizeof(ft4));
			setPolyFT4(&ft4);
			setRGB0(&ft4, colors[0].r, colors[0].g, colors[0].b);
			assert(primitive.uvs.size() == 4);
			setUV4(&ft4,
			       primitive.uvs[0].u, primitive.uvs[0].v,
			       primitive.uvs[1].u, primitive.uvs[1].v,
			       primitive.uvs[2].u, primitive.uvs[2].v,
			       primitive.uvs[3].u, primitive.uvs[3].v);
			ft4.clut = primitive.clut;
			ft4.tpage = primitive.tpage;
			setSemiTrans(&ft4, primitive.has_translucency);
			setShadeTex(&ft4, primitive.has_brightness);
			write_sizeof<POLY_FT4>(os, ft4);
			write_sizeof<POLY_FT4>(os, ft4);
		}
		break;
	case 2:
		{
			POLY_GT3 gt3;
			std::memset(&gt3, 0, sizeof(gt3));
			setPolyGT3(&gt3);
			setRGB0(&gt3, colors[0].r, colors[0].g, colors[0].b);
			setRGB1(&gt3, colors[1].r, colors[1].g, colors[1].b);
			setRGB2(&gt3, colors[2].r, colors[2].g, colors[2].b);
			setUV3(&gt3,
			       primitive.uvs[0].u, primitive.uvs[0].v,
			       primitive.uvs[1].u, primitive.uvs[1].v,
			       primitive.uvs[2].u, primitive.uvs[2].v);
			gt3.clut = primitive.clut;
			gt3.tpage = primitive.tpage;
			setSemiTrans(&gt3, primitive.has_translucency);
			setShadeTex(&gt3, primitive.has_brightness);
			write_sizeof<POLY_GT3>(os, gt3);
			write_sizeof<POLY_GT3>(os, gt3);
		}
		break;
	case 3:
		{
			POLY_GT4 gt4;
			std::memset(&gt4, 0, sizeof(gt4));
			setPolyGT4(&gt4);
			setRGB0(&gt4, colors[0].r, colors[0].g, colors[0].b);
			setRGB1(&gt4, colors[1].r, colors[1].g, colors[1].b);
			setRGB2(&gt4, colors[2].r, colors[2].g, colors[2].b);
			setRGB3(&gt4, colors[3].r, colors[3].g, colors[3].b);
			setUV4(&gt4,
			       primitive.uvs[0].u, primitive.uvs[0].v,
			       primitive.uvs[1].u, primitive.uvs[1].v,
			       primitive.uvs[2].u, primitive.uvs[2].v,
			       primitive.uvs[3].u, primitive.uvs[3].v);
			gt4.clut = primitive.clut;
			gt4.tpage = primitive.tpage;
			setSemiTrans(&gt4, primitive.has_translucency);
			setShadeTex(&gt4, primitive.has_brightness);
			write_sizeof<POLY_GT4>(os, gt4);
			write_sizeof<POLY_GT4>(os, gt4);
		}
		break;
	case 4:
		{
			POLY_F3 f3;
			std::memset(&f3, 0, sizeof(f3));
			setPolyF3(&f3);
			setRGB0(&f3, colors[0].r, colors[0].g, colors[0].b);
			setSemiTrans(&f3, primitive.has_translucency);
			setShadeTex(&f3, primitive.has_brightness);
			write_sizeof<POLY_F3>(os, f3);
			write_sizeof<POLY_F3>(os, f3);
		}
		break;
	case 5:
		{
			POLY_F4 f4;
			std::memset(&f4, 0, sizeof(f4));
			setPolyF4(&f4);
			setRGB0(&f4, colors[0].r, colors[0].g, colors[0].b);
			setSemiTrans(&f4, primitive.has_translucency);
			setShadeTex(&f4, primitive.has_brightness);
			write_sizeof<POLY_F4>(os, f4);
			write_sizeof<POLY_F4>(os, f4);
		}
		break;
	case 6:
		{
			POLY_G3 g3;
			setPolyG3(&g3);
			setRGB0(&g3, colors[0].r, colors[0].g, colors[0].b);
			setRGB1(&g3, colors[1].r, colors[1].g, colors[1].b);
			setRGB2(&g3, colors[2].r, colors[2].g, colors[2].b);
			setSemiTrans(&g3, primitive.has_translucency);
			setShadeTex(&g3, primitive.has_brightness);
			write_sizeof<POLY_G3>(os, g3);
			write_sizeof<POLY_G3>(os, g3);
		}
		break;
	case 7:
		{
			POLY_G4 g4;
			std::memset(&g4, 0, sizeof(g4));
			setPolyG4(&g4);
			setRGB0(&g4, colors[0].r, colors[0].g, colors[0].b);
			setRGB1(&g4, colors[1].r, colors[1].g, colors[1].b);
			setRGB2(&g4, colors[2].r, colors[2].g, colors[2].b);
			setRGB3(&g4, colors[3].r, colors[3].g, colors[3].b);
			setSemiTrans(&g4, primitive.has_translucency);
			setShadeTex(&g4, primitive.has_brightness);
			write_sizeof<POLY_G4>(os, g4);
			write_sizeof<POLY_G4>(os, g4);
		}
		break;
	}

	return os;
}

struct pmd_type_cmp
{
	bool operator()(uint16_t a, uint16_t b) const
	{
		if (((a & 1) == 0) && ((b & 1) == 1))
		{
			return true;
		}
		else if (((a & 1) == 1) && ((b & 1) == 0))
		{
			return false;
		}
		else
		{
			return a < b;
		}
	}
};

class pmd_object
{
public:
	std::vector<svector> vertices;
	std::vector<svector> normals;
	std::map<uint16_t, std::vector<pmd_primitive>, pmd_type_cmp> primitives;
	bool indexed;

	pmd_object(const tmd_object& object, bool shared);

	void write_primitives(std::ostream& os, uint16_t type) const;

	friend std::ostream& operator<<(std::ostream& os,
					const pmd_object& object);
};

pmd_object::pmd_object(const tmd_object& object, bool shared)
	: vertices(object.vertices),
	  normals(object.normals),
	  indexed(shared)
{
	for (const tmd_primitive& primitive : object.primitives)
	{
		pmd_primitive p(primitive, shared);
		primitives[p.type].push_back(p);
	}
}

void pmd_object::write_primitives(std::ostream& os, uint16_t type) const
{
	write_sizeof<uint32_t>(os, type << 16 | primitives.at(type).size());

	for (const pmd_primitive& primitive : primitives.at(type))
	{
		os << primitive;
		for (uint16_t i : primitive.vertex_indices)
		{
			if (indexed)
			{
				write_sizeof<uint32_t>(os, i);
			}
			else
			{
				write_sizeof<svector>(os, vertices[i]);
			}
		}
	}

	for (const pmd_primitive& primitive : primitives.at(type))
	{
		for (uint16_t i : primitive.normal_indices)
		{
			if (indexed)
			{
				write_sizeof<uint32_t>(os, i);
			}
			else
			{
				write_sizeof<svector>(os, normals[i]);
			}
		}
	}

	for (const pmd_primitive& primitive : primitives.at(type))
	{
		for (const cvector& color : primitive.colors)
		{
			write_sizeof<cvector>(os, color);
		}
	}
}

class pmd_model
{
public:
	std::vector<pmd_object> objects;
	bool indexed;

	pmd_model(const tmd_model& model, bool shared);

	friend std::ostream& operator<<(std::ostream& os,
					const pmd_model& model);
};

pmd_model::pmd_model(const tmd_model& model, bool shared)
	: indexed(shared)
{
	for (const tmd_object& object : model.objects)
	{
		objects.emplace_back(object, shared);
	}
}

std::ostream& operator<<(std::ostream& os, const pmd_model& model)
{
	std::map<size_t, std::vector<uint32_t>> primitive_offsets;
	std::set<uint16_t, pmd_type_cmp> primitive_types;
	uint32_t prim_point;
	uint32_t vert_point;

	write_sizeof<uint32_t>(os, 0);
	write_sizeof<uint32_t>(os, 0);
	write_sizeof<uint32_t>(os, 0);
	write_sizeof<uint32_t>(os, 0);

	for (const pmd_object& object : model.objects)
	{
		write_sizeof<uint32_t>(os, 0);

		auto& primitives = object.primitives;
		for (auto it = primitives.begin(); it != primitives.end(); ++it)
		{
			primitive_types.insert(it->first);
			write_sizeof<uint32_t>(os, 0);
		}
		for (size_t i = 0; i < object.primitives.size(); ++i)
		{
		}
	}

	prim_point = (uint32_t)os.tellp();

	for (uint16_t type : primitive_types)
	{
		for (size_t i = 0; i < model.objects.size(); ++i)
		{
			const pmd_object& object = model.objects[i];
			auto& primitives = object.primitives;
			for (auto it = primitives.begin(); it != primitives.end(); ++it)
			{
				if (it->first == type)
				{
					primitive_offsets[i].push_back((uint32_t)os.tellp());
					object.write_primitives(os, it->first);
				}
			}
		}
	}

	vert_point = model.indexed ? (uint32_t)os.tellp() : 0;

	os.seekp(0);
	write_sizeof<uint32_t>(os, 0x42);
	write_sizeof<uint32_t>(os, prim_point);
	write_sizeof<uint32_t>(os, vert_point);
	write_sizeof<uint32_t>(os, model.objects.size());

	for (size_t i = 0; i < model.objects.size(); ++i)
	{
		write_sizeof<uint32_t>(os, model.objects[i].primitives.size());
		for (size_t j = 0; j < primitive_offsets[i].size(); ++j)
		{
			write_sizeof<uint32_t>(os, primitive_offsets[i][j]);
		}
	}

	return os;
}

static void usage(const char *argv0)
{
	std::cout << "usage: " << argv0;
	std::cout << " [-s] <TMD file> <PMD file>" << std::endl;
}

int main(int argc, char *argv[])
{
	const char *argv0 = argv[0];
	bool shared = false;

	if (argc < 3)
	{
		usage(argv0);
		return 1;
	}

	for (--argc, ++argv; argc; --argc, ++argv)
	{
		if (**argv == '-')
		{
			for (++*argv; **argv; ++*argv)
			{
				switch (**argv)
				{
				case 's':
					shared = true;
					break;
				default:
					usage(argv0);
					return 1;
				}
			}
		}
		else
		{
			break;
		}
	}

	if (argc != 2)
	{
		usage(argv0);
		return 1;
	}

	std::ifstream ifs(argv[0], std::ios::binary);
	ifs.exceptions(std::ifstream::badbit | std::ifstream::failbit);

	tmd_model tmd(ifs);

	ifs.close();

	pmd_model pmd(tmd, shared);

	std::ofstream ofs(argv[1], std::ios::binary);
	ofs.exceptions(std::ofstream::badbit | std::ofstream::failbit);

	ofs << pmd;

	ofs.close();

	return 0;
}
