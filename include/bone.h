/* See LICENSE file for copyright and license details. */

#ifndef DW_BONE_H
#define DW_BONE_H

#include <stdint.h>

#include "utils.h"

typedef struct {
	uint8_t object;
	uint8_t parent;
} dw_bone;

DW_STATIC_ASSERT(sizeof(dw_bone) == 2);

#endif
