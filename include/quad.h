/* See LICENSE file for copyright and license details. */

#ifndef DW_QUAD_H
#define DW_QUAD_H

#include <stdint.h>

#include "pmd.h"

typedef enum {
	DW_PLANE_XY,
	DW_PLANE_XZ,
	DW_PLANE_YZ,
} dw_plane;

typedef struct {
	pmd_object object;
	uint32_t *ptr;
	uint32_t type;
	pmd_poly_ft4 pft4;
} dw_quad_ft4;

void dw_quad_ft4_init(dw_quad_ft4 *qft4,
		      dw_plane plane,
		      int16_t x, int16_t y,
		      int16_t w, int16_t h);

#endif
