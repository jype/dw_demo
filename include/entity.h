/* See LICENSE file for copyright and license details. */

#ifndef DW_ENTITY_H
#define DW_ENTITY_H

#include <stdint.h>

#include <sys/types.h>

#include <libgte.h>

#include "mtn.h"
#include "pmd.h"
#include "quad.h"
#include "skeleton.h"

typedef struct {
	VECTOR position;
	SVECTOR rotation;
	VECTOR scale;
	dw_skeleton *skeleton;
	pmd_model *model;
	dw_mtn *mtn;
	dw_quad_ft4 shadow;
	uint16_t character_id;
} dw_entity;

dw_entity *dw_entity_init(uint8_t character_id, uint8_t slot);
void dw_entity_deinit(dw_entity *ent);

void dw_entity_look_at(dw_entity *ent, VECTOR *v);

void dw_entity_play_anim(dw_entity *ent, uint8_t anim_id);

void dw_entity_animate(dw_entity *ent);
void dw_entity_update(dw_entity *ent);

void dw_entity_sort(dw_entity *ent, u_long *ot, int otlen, int id);

#endif
