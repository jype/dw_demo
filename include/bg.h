/* See LICENSE file for copyright and license details. */

#ifndef DW_BG_H
#define DW_BG_H

#include <stdint.h>

#include <sys/types.h>

#include "map.h"
#include "tfs.h"

typedef struct dw_bg dw_bg;

dw_bg *dw_bg_init(const dw_map *map, const dw_tfs *tfs,
		  uint16_t display_width, uint16_t display_height);
void dw_bg_deinit(dw_bg *bg);

void dw_bg_get_position(dw_bg *bg, int16_t *x, int16_t *y);

void dw_bg_get_center(dw_bg *bg, int16_t *x, int16_t *y);
void dw_bg_set_center(dw_bg *bg, int16_t x, int16_t y);

void dw_bg_update(dw_bg *bg, int id);

void dw_bg_sort(dw_bg *bg, u_long *ot, int id);

#endif
