/* See LICENSE file for copyright and license details. */

#ifndef DW_MTN_H
#define DW_MTN_H

#include <stdbool.h>
#include <stdint.h>

#include <sys/types.h>

#include <libgte.h>
#include <libgpu.h>

#include "skeleton.h"

typedef struct dw_mtn dw_mtn;

dw_mtn *dw_mtn_init(uint8_t *mtn_data, uint8_t num_nodes, RECT *rect);

void dw_mtn_deinit(dw_mtn *mtn);

uint8_t dw_mtn_get_num_anim(const dw_mtn *mtn);

uint16_t dw_mtn_get_frame(const dw_mtn *mtn);

bool dw_mtn_anim_ended(const dw_mtn *mtn);

void dw_mtn_get_root_motion(const dw_mtn *mtn, VECTOR *v);

void dw_mtn_play(dw_mtn *mtn, uint8_t anim_id);

void dw_mtn_run(dw_mtn *mtn);

void dw_mtn_apply(const dw_mtn *mtn, dw_skeleton *skeleton);

#endif
