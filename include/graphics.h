/* See LICENSE file for copyright and license details. */

#ifndef DW_GRAPHICS_H
#define DW_GRAPHICS_H

#include <stdint.h>
#include <stdio.h>

#include <sys/types.h>

#include <libgte.h>
#include <libgpu.h>

#include "inline_n.h"
#include "pad.h"

extern const MATRIX dw_identity_matrix;
extern MATRIX dw_world_screen_matrix;
extern MATRIX dw_light_source_matrix;
extern MATRIX dw_light_color_matrix;
extern MATRIX dw_light_matrix;

extern DRAWENV dw_draw[2];
extern DISPENV dw_disp[2];

static inline void dw_gte_rtps_sxy(const SVECTOR *v, int16_t *sxy)
{
	gte_ldv0(v);
	gte_rtps();
	gte_stsxy(sxy);
}

void db_init(int w, int h, int z, u_char r0, u_char g0, u_char b0);
void db_font_init(void);
void db_swap(u_long *ot, int font_enabled);

extern controller_packet pad_port1;
extern controller_packet pad_port2;

extern int32_t fgfont;
extern int32_t bgfont;

static inline void dump_POLY_FT4(const POLY_FT4 *ft4)
{
	printf("POLY_FT4\n");
	printf("tag %08x\n", ft4->tag);
	printf("r0 g0 b0 code %02x %02x %02x %02x\n", ft4->r0, ft4->g0, ft4->b0, ft4->code);
	printf("x0 y0 %04x %04x\n", (uint16_t)ft4->x0, (uint16_t)ft4->y0);
	printf("u0 v0 clut %02x %02x %04x\n", ft4->u0, ft4->v0, ft4->clut);
	printf("x1 y1 %04x %04x\n", (uint16_t)ft4->x1, (uint16_t)ft4->y1);
	printf("u1 v1 tpage %02x %02x %04x\n", ft4->u1, ft4->v1, ft4->tpage);
	printf("x2 y2 %04x %04x\n", (uint16_t)ft4->x2, (uint16_t)ft4->y2);
	printf("u2 v2 pad1 %02x %02x %04x\n", ft4->u2, ft4->v2, ft4->pad1);
	printf("x3 y3 %04x %04x\n", (uint16_t)ft4->x3, (uint16_t)ft4->y3);
	printf("u3 v3 pad2 %02x %02x %04x\n", ft4->u3, ft4->v3, ft4->pad2);
}

static inline void dump_LINE_F2(const LINE_F2 *f2)
{
	printf("LINE_F2\n");
	printf("tag %08x\n", f2->tag);
	printf("r0 g0 b0 code %02x %02x %02x %02x\n", f2->r0, f2->g0, f2->b0, f2->code);
	printf("x0 y0 %04x %04x\n", (uint16_t)f2->x0, (uint16_t)f2->y0);
	printf("x1 y1 %04x %04x\n", (uint16_t)f2->x1, (uint16_t)f2->y1);
}

static inline void dump_TILE(const TILE *tile)
{
	printf("TILE\n");
	printf("tag %08x\n", tile->tag);
	printf("r0 g0 b0 code %02x %02x %02x %02x\n", tile->r0, tile->g0, tile->b0, tile->code);
	printf("x0 y0 %04x %04x\n", (uint16_t)tile->x0, (uint16_t)tile->y0);
	printf("w h %04x %04x\n", (uint16_t)tile->w, (uint16_t)tile->h);
}

#endif
