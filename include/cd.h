/* See LICENSE file for copyright and license details. */

#ifndef DW_CD_H
#define DW_CD_H

#include <stdint.h>

#include <sys/types.h>

#include <libds.h>

#define CD_SECTOR_SIZE		((SECTOR_SIZE) * sizeof(uint32_t))

#define BYTES_TO_SECTORS(n) \
	(((n) + (CD_SECTOR_SIZE) - 1) / (CD_SECTOR_SIZE))

void cd_read_sectors(const char *file, uint8_t *buf, int sector_offset,
		     int num_sectors);

void *cd_read_alloc(const char *file, size_t *size);

#endif
