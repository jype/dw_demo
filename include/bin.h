/* See LICENSE file for copyright and license details. */

#ifndef DW_BIN_H
#define DW_BIN_H

#include <stdint.h>

#define DECLARE_BIN(name)			\
	extern const uint8_t name ## _start[];	\
	extern const uint8_t name ## _end[];

DECLARE_BIN(font);
DECLARE_BIN(font_clut);
DECLARE_BIN(character_params);
DECLARE_BIN(character_model_name);
DECLARE_BIN(skeleton_node_offset);
DECLARE_BIN(skeleton_node);
DECLARE_BIN(map_entry);
DECLARE_BIN(map_loading_name_offset);
DECLARE_BIN(map_loading_name);
DECLARE_BIN(map_toilet);
DECLARE_BIN(map_doors);

#endif
