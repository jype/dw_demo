/* See LICENSE file for copyright and license details. */

#ifndef DW_PARAMS_H
#define DW_PARAMS_H

#include <stdint.h>

#include "utils.h"

#define DW_NUM_CHARACTER_PARAMS		180

typedef struct {
	char name[20];
	int32_t num_bones;
	int16_t radius;
	int16_t height;
	uint8_t type;
	uint8_t level;
	uint8_t special[3];
	uint8_t drop_item;
	uint8_t drop_chance;
	int8_t moves[16];
	uint8_t pad;
} dw_params;

DW_STATIC_ASSERT(sizeof(dw_params) == 52);

dw_params *dw_get_character_params(uint8_t id);

const char *dw_get_character_model_name(uint8_t id);

#endif
