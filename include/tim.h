/* See LICENSE file for copyright and license details. */

#ifndef DW_TIM_H
#define DW_TIM_H

#include <sys/types.h>

#include <libgte.h>
#include <libgpu.h>

void dw_tim_load(u_long *addr);

void dw_tim_load_rect(u_long *addr, RECT *prect, RECT *crect);

#endif
