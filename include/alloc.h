/* See LICENSE file for copyright and license details. */

#ifndef DW_ALLOC_H
#define DW_ALLOC_H

#include <stddef.h>

#ifdef DW_DEBUG_ALLOC
#define DW_MAX_ALLOCS	256

#define xmalloc(size) \
	(debug_malloc((size), __FILE__, __LINE__))
#define xcalloc(nmemb, size) \
	(debug_calloc((nmemb), (size), __FILE__, __LINE__))
#define xrealloc(ptr, size) \
	(debug_realloc((ptr), (size), __FILE__, __LINE__))
#define xfree(ptr) \
	(debug_free((ptr), __FILE__, __LINE__))

void *debug_malloc(size_t size, const char *file, int line);
void *debug_calloc(size_t nmemb, size_t size, const char *file, int line);
void *debug_realloc(void *ptr, size_t size, const char *file, int line);
void debug_free(void *ptr, const char *file, int line);

void reset_allocs(void);
void check_allocs(void);
#else
void *xmalloc(size_t size);
void *xcalloc(size_t nmemb, size_t size);
void *xrealloc(void *ptr, size_t size);
void xfree(void *ptr);

static inline void reset_allocs(void) {}
static inline void check_allocs(void) {}
#endif

#endif
