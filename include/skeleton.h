/* See LICENSE file for copyright and license details. */

#ifndef DW_SKELETON_H
#define DW_SKELETON_H

#include <stdint.h>

#include <sys/types.h>

#include <libgte.h>

#include "bone.h"
#include "pmd.h"
#include "utils.h"

typedef struct {
	VECTOR position;
	SVECTOR rotation;
	VECTOR scale;
	MATRIX lw;
	MATRIX ls;
	pmd_object *object;
} dw_node;

typedef struct {
	const dw_bone *root;
	uint32_t num_nodes;
	dw_node nodes[0];
} dw_skeleton;

dw_skeleton *dw_skeleton_init(uint8_t character_id);
void dw_skeleton_deinit(dw_skeleton *skeleton);

#endif
