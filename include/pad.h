/* See LICENSE file for copyright and license details. */

#ifndef DW_PAD_H
#define DW_PAD_H

#include "utils.h"

/* No pad or bad transmission */
#define BAD_TRANSMISSION	0xff

/* No digital key pressed */
#define NOKEY			0xffff

#define MAX_CONTROLLER_BYTES	34

#define STD_PAD_DATA		1
#define MOUSE_DATA		2
#define NEGCON_DATA		3
#define ANALOG_JOY_DATA		8
#define MULTI_TAP_DATA		16
#define GUNCON_DATA		3

#define MOUSE			1
#define NEGCON			2
#define STD_PAD			4
#define ANALOG_JOY		5
#define GUNCON			6
#define MULTI_TAP		8

typedef unsigned short pad_data;

#define PAD_R1			0x0800
#define PAD_L1			0x0400
#define PAD_R2			0x0200
#define PAD_L2			0x0100

#define PAD_RUP			0x1000
#define PAD_RDOWN		0x4000
#define PAD_RLEFT		0x8000
#define PAD_RRIGHT		0x2000

#define PAD_LUP			0x0010
#define PAD_LDOWN		0x0040
#define PAD_LLEFT		0x0080
#define PAD_LRIGHT		0x0020

#define PAD_SELECT		0x0001
#define PAD_START		0x0008

#define PAD_KEY_IS_PRESSED(x,y)	(!((x)->data.pad & (y)))

#define PAD_KEY_PRESSED(x)	((x)->data.pad != NOKEY)
#define PAD_NO_KEY_PRESSED(x)	((x)->data.pad == NOKEY)

typedef struct
{
	/* Bit mask of plain keys */
	unsigned short digital_buttons;
	unsigned char  right_x;
	unsigned char  right_y;
	unsigned char  left_x;
	unsigned char  left_y;
} joystick_data;

#define JOY_RIGHT_X(x)		((x)->data.joystick.right_x)
#define JOY_RIGHT_Y(x)		((x)->data.joystick.right_y)

#define JOY_LEFT_X(x)		((x)->data.joystick.left_x)
#define JOY_LEFT_Y(x)		((x)->data.joystick.left_y)

#define JOY_KEY_IS_PRESSED(x,y)	(!((x)->data.joystick.digital_buttons & (y)))
#define JOY_KEY_PRESSED(x)	((x)->data.joystick.digital_buttons != NOKEY)
#define JOY_NO_KEY_PRESSED(x)	((x)->data.joystick.digital_buttons == NOKEY)

typedef struct {
	/* 0xff = no pad, bad pad, bad transmission */
	unsigned char trans_status;
	/* Top 4 bits = type of controller */
	/* Bottom 4 bits = shorts of data written */
	unsigned char data_format;
	union {
		char data[MAX_CONTROLLER_BYTES - 2];
		pad_data pad;
		joystick_data joystick;
	} data;
} controller_packet;

DW_STATIC_ASSERT(sizeof(controller_packet) == 34);

#define PAD_DATA_VALID(x)	((x)->trans_status != 0xff)

#define PAD_GET_TYPE(x)		(((x)->data_format & 0xf0) >> 4)

#endif
