/* See LICENSE file for copyright and license details. */

#ifndef DW_TFS_H
#define DW_TFS_H

#include <stdint.h>

#include "utils.h"

typedef struct {
	uint16_t data[256];
} dw_tfs_palette;

DW_STATIC_ASSERT(sizeof(dw_tfs_palette) == 512);

typedef struct {
	uint16_t position_x;
	uint16_t position_y;
	uint8_t data[128 * 128];
} dw_tfs_image;

DW_STATIC_ASSERT(sizeof(dw_tfs_image) == 16388);

typedef struct {
	uint16_t width;
	uint16_t height;
	uint32_t num_palettes;
#ifndef __cplusplus
	dw_tfs_palette palettes[0];
#endif
} dw_tfs_config;

DW_STATIC_ASSERT(sizeof(dw_tfs_config) == 8);

typedef struct {
	uint8_t *tfs_data;
	dw_tfs_config *config;
	dw_tfs_image *images;
	uint32_t num_images;
} dw_tfs;

dw_tfs *dw_tfs_init(uint8_t map_id);
void dw_tfs_deinit(dw_tfs *tfs);

#endif
