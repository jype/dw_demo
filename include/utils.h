/* See LICENSE file for copyright and license details. */

#ifndef DW_UTILS_H
#define DW_UTILS_H

#ifdef __cplusplus
#define DW_STATIC_ASSERT(...)	static_assert(__VA_ARGS__, "")
#else
#define DW_STATIC_ASSERT(...)	_Static_assert(__VA_ARGS__, "")
#endif

#define DW_ABS(x)		(((x) >= 0) ? (x) : (-(x)))
#define DW_MIN(a, b)		(((a) < (b)) ? (a) : (b))
#define DW_MAX(a, b)		(((a) > (b)) ? (a) : (b))
#define DW_CLAMP(a, min, max)	(DW_MIN(DW_MAX(a, min), max))

#define DW_ARRAY_SIZE(a)	(sizeof(a) / sizeof(*(a)))

#define DW_DEBUG_ASSERT(expr) \
	((expr) ? (void)0 : debug_assert_fail(#expr, __FILE__, __LINE__))

void debug_assert_fail(const char *assertion, const char *file, int line);

#endif
