/* See LICENSE file for copyright and license details. */

#ifndef DW_GS_H
#define DW_GS_H

#include <sys/types.h>

#include <libgte.h>

void gs_set_ref_view(MATRIX *m, const VECTOR *vp, const VECTOR *vr);

#endif
