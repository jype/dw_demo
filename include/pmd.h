/* See LICENSE file for copyright and license details. */

#ifndef DW_PMD_H
#define DW_PMD_H

#include <stdint.h>

#include <sys/types.h>

#include <libgte.h>
#include <libgpu.h>

#define PMD_NPACKET_SHIFT	0
#define PMD_NPACKET_MASK	0xffff
#define PMD_NPACKET(n) \
	(((n) >> (PMD_NPACKET_SHIFT)) & (PMD_NPACKET_MASK))

#define PMD_TYPE_SHIFT		16
#define PMD_TYPE_MASK		0xffff
#define PMD_TYPE(n)		(((n) >> (PMD_TYPE_SHIFT)) & (PMD_TYPE_MASK))

#define PMD_TYPE_QUAD		(1 << 0)
#define PMD_TYPE_GOURAUD	(1 << 1)
#define PMD_TYPE_NO_TEXTURE	(1 << 2)
#define PMD_TYPE_SHARED		(1 << 3)
#define PMD_TYPE_LIGHT		(1 << 4)
#define PMD_TYPE_NO_BACK_CLIP	(1 << 5)

#define PMD_POLY_TYPE_MASK	0x7
#define PMD_POLY_TYPE(n)	(PMD_TYPE(n) & (PMD_POLY_TYPE_MASK))

#define PMD_POLY_TYPE_FT3	0
#define PMD_POLY_TYPE_FT4	1
#define PMD_POLY_TYPE_GT3	2
#define PMD_POLY_TYPE_GT4	3
#define PMD_POLY_TYPE_F3	4
#define PMD_POLY_TYPE_F4	5
#define PMD_POLY_TYPE_G3	6
#define PMD_POLY_TYPE_G4	7

#define PMD_NUM_POLY_TYPES	((PMD_POLY_TYPE_MASK) + 1)

#define PMD_MK_TYPE(npacket, type) \
	((((npacket) & (PMD_NPACKET_MASK)) << (PMD_NPACKET_SHIFT)) | \
	 (((type) & (PMD_TYPE_MASK)) << (PMD_TYPE_SHIFT)))

/* TYPE=00 (Triangle/Flat/Texture-On/Independent vertex) */
typedef struct {
	POLY_FT3 pkt[2];
	SVECTOR v1, v2, v3;
} pmd_poly_ft3;

/* TYPE=01 (Quadrangle/Flat/Texture-On/Independent vertex) */
typedef struct {
	POLY_FT4 pkt[2];
	SVECTOR v1, v2, v3, v4;
} pmd_poly_ft4;

/* TYPE=02 (Triangle/Gouraud/Texture-On/Independent vertex) */
typedef struct {
	POLY_GT3 pkt[2];
	SVECTOR v1, v2, v3;
} pmd_poly_gt3;

/* TYPE=03 (Quadrangle/Gouraud/Texture-On/Independent vertex) */
typedef struct {
	POLY_GT4 pkt[2];
	SVECTOR v1, v2, v3, v4;
} pmd_poly_gt4;

/* TYPE=04 (Triangle/Flat/Texture-Off/Independent vertex) */
typedef struct {
	POLY_F3 pkt[2];
	SVECTOR v1, v2, v3;
} pmd_poly_f3;

/* TYPE=05 (Quadrangle/Flat/Texture-Off/Independent vertex) */
typedef struct {
	POLY_F4 pkt[2];
	SVECTOR v1, v2, v3, v4;
} pmd_poly_f4;

/* TYPE=06 (Triangle/Gouraud/Texture-Off/Independent vertex) */
typedef struct {
	POLY_G3 pkt[2];
	SVECTOR v1, v2, v3;
} pmd_poly_g3;

/* TYPE=07 (Quadrangle/Gouraud/Texture-Off/Independent vertex) */
typedef struct {
	POLY_G4 pkt[2];
	SVECTOR v1, v2, v3, v4;
} pmd_poly_g4;

/* TYPE=08 (Triangle/Flat/Texture-On/Shared vertex) */
typedef struct {
	POLY_FT3 pkt[2];
	long vp1, vp2, vp3;
} pmd_poly_ft3c;

/* TYPE=09 (Quadrangle/Flat/Texture-On/Shared vertex) */
typedef struct {
	POLY_FT4 pkt[2];
	long vp1, vp2, vp3, vp4;
} pmd_poly_ft4c;

/* TYPE=0a (Triangle/Gouraud/Texture-On/Shared vertex) */
typedef struct {
	POLY_GT3 pkt[2];
	long vp1, vp2, vp3;
} pmd_poly_gt3c;

/* TYPE=0b (Quadrangle/Gouraud/Texture-On/Shared vertex) */
typedef struct {
	POLY_GT4 pkt[2];
	long vp1, vp2, vp3, vp4;
} pmd_poly_gt4c;

/* TYPE=0c (Triangle/Flat/Texture-Off/Shared vertex) */
typedef struct {
	POLY_F3 pkt[2];
	long vp1, vp2, vp3;
} pmd_poly_f3c;

/* TYPE=0d (Quadrangle/Flat/Texture-Off/Shared vertex) */
typedef struct {
	POLY_F4 pkt[2];
	long vp1, vp2, vp3, vp4;
} pmd_poly_f4c;

/* TYPE=0e (Triangle/Gouraud/Texture-Off/Shared vertex) */
typedef struct {
	POLY_G3 pkt[2];
	long vp1, vp2, vp3;
} pmd_poly_g3c;

/* TYPE=0f (Quadrangle/Gouraud/Texture-Off/Shared vertex) */
typedef struct {
	POLY_G4 pkt[2];
	long vp1, vp2, vp3, vp4;
} pmd_poly_g4c;

typedef struct {
	uint32_t nptr;
	uint32_t *ptr[0];
} pmd_object;

_Static_assert(sizeof(pmd_object) == 4);

typedef struct {
	uint32_t id;
	uint32_t prim_point;
	uint32_t vert_point;
	uint32_t nobj;
	uint32_t object0;
} pmd_model;

_Static_assert(sizeof(pmd_model) == 20);

void pmd_init(pmd_model *pmd);

pmd_object *pmd_get_object(pmd_model *pmd, uint32_t obj_no);

void pmd_object_set_texture(pmd_object *object,
			    uint16_t tx, uint16_t ty,
			    uint16_t cdx, uint16_t cdy);

void pmd_object_sort(pmd_object *object, u_long *ot, int otlen, int id);

#endif
