/* See LICENSE file for copyright and license details. */

#ifndef DW_MAP_H
#define DW_MAP_H

#include <stdint.h>

#include "utils.h"

#define DW_NUM_MAP_ENTRIES		256

typedef struct {
	char name[10];
	uint8_t num_images;
	uint8_t num_objects;
	uint8_t flags;
	uint8_t doors_id;
	uint8_t toilet_id;
	uint8_t loading_name_id;
} dw_map_entry;

DW_STATIC_ASSERT(sizeof(dw_map_entry) == 16);

typedef struct {
	int16_t x1;
	int16_t y1;
	int16_t x2;
	int16_t y2;
} dw_map_toilet;

DW_STATIC_ASSERT(sizeof(dw_map_toilet) == 8);

typedef struct {
	uint8_t model_id[6];
	uint8_t position_x[6];
	uint8_t position_y[6];
	uint8_t position_z[6];
	uint8_t rotation[6];
} dw_map_doors;

DW_STATIC_ASSERT(sizeof(dw_map_doors) == 30);

typedef struct {
	int32_t vx, vy, vz;
	uint32_t r, g, b;
} dw_map_light;

DW_STATIC_ASSERT(sizeof(dw_map_light) == 24);

typedef struct {
	int32_t vpx, vpy, vpz;
	int32_t vrx, vry, vrz;
	dw_map_light lights[3];
	uint32_t ambient_r;
	uint32_t ambient_g;
	uint32_t ambient_b;
	uint32_t distance;
	int32_t liked_area[4];
	int32_t disliked_area[4];
	uint32_t width;
	uint32_t height;
#ifndef __cplusplus
	uint32_t tiles[0];
#endif
} dw_map_config;

DW_STATIC_ASSERT(sizeof(dw_map_config) == 152);

typedef struct {
	uint16_t uv_x;
	uint16_t uv_y;
	uint16_t width;
	uint16_t height;
	int16_t position_x;
	int16_t position_y;
	int16_t position_z;
	uint16_t clut;
	uint16_t transparency;
} dw_map_object;

DW_STATIC_ASSERT(sizeof(dw_map_object) == 18);

typedef struct {
	uint16_t num_objects;
#ifndef __cplusplus
	dw_map_object objects[0];
#endif
} dw_map_objects;

DW_STATIC_ASSERT(sizeof(dw_map_objects) == 2);

typedef struct {
	int16_t anim_state[8];
	int16_t anim_duration[8];
	uint16_t position_x;
	uint16_t position_y;
	uint16_t flag;
} dw_map_instance;

DW_STATIC_ASSERT(sizeof(dw_map_instance) == 38);

typedef struct {
	uint16_t num_instances;
#ifndef __cplusplus
	dw_map_instance instances[0];
#endif
} dw_map_instances;

DW_STATIC_ASSERT(sizeof(dw_map_instances) == 2);

typedef struct {
	uint16_t type;
	uint16_t ai_type;

	int16_t position_x;
	int16_t position_y;
	int16_t position_z;
	int16_t rotation_x;
	int16_t rotation_y;
	int16_t rotation_z;

	uint16_t tracking_range;
	uint16_t unknown0;
	uint8_t script_id;
	uint8_t unknown1;

	uint16_t hp;
	uint16_t mp;
	uint16_t max_hp;
	uint16_t max_mp;
	uint16_t offense;
	uint16_t defense;
	uint16_t speed;
	uint16_t brains;

	uint16_t bits;
	uint16_t charge_mode;
	uint16_t unknown3;

	uint16_t moves[4];
	uint16_t move_weights[4];

	int16_t flee_position_x;
	int16_t flee_position_y;
	int16_t flee_position_z;

	uint16_t num_waypoints;
	int16_t waypoint_speed[8];
#ifndef __cplusplus
	int16_t waypoints[0];
#endif
} dw_map_digimon;

DW_STATIC_ASSERT(sizeof(dw_map_digimon) == 84);

typedef struct {
	int16_t spawn_x[10];
	int16_t spawn_y[10];
	int16_t spawn_z[10];
	int16_t spawn_rotation[10];

	uint16_t warp_target_map[10];
	uint16_t warp_target_spawn[10];

	uint16_t num_digimon;
#ifndef __cplusplus
	dw_map_digimon digimons[0];
#endif
} dw_map_elements;

DW_STATIC_ASSERT(sizeof(dw_map_elements) == 122);

typedef struct {
	uint8_t *map_data;
	dw_map_entry *entry;
	dw_map_toilet *toilet;
	dw_map_doors *doors;
	dw_map_config *config;
	dw_map_objects *objects;
	dw_map_instances *instances;
	dw_map_elements *elements;
	uint8_t *tile_map;
} dw_map;

dw_map_entry *dw_get_map_entry(uint8_t map_id);
dw_map_toilet *dw_get_map_toilet(uint8_t toilet_id);
dw_map_doors *dw_get_map_doors(uint8_t doors_id);
const char *dw_get_map_loading_name(uint8_t loading_name_id);

dw_map *dw_map_init(uint8_t map_id);
void dw_map_deinit(dw_map *map);

#ifndef __cplusplus
void dw_map_get_light_source_matrix(const dw_map *map, MATRIX *m);
void dw_map_get_light_color_matrix(const dw_map *map, MATRIX *m);
#endif

#endif
