#!/bin/sh
# See LICENSE file for copyright and license details.

set -ex

usage() {
	printf 'usage: %s <path to extracted ISO>\n' "$0"
}

ISODIR="$1"
[ -z "$ISODIR" ] && { usage; exit 1; }

DIR="$(dirname "$(readlink -f "$0")")"
BUILDDIR="$DIR/build"
DATADIR="$BUILDDIR/data"

BIN="$ISODIR/SLUS_010.32"

make -f "$DIR/tools/extract_exe/Makefile"

rm -rf "$DATADIR"
mkdir -p "$DATADIR"

"$BUILDDIR/extract_exe" "$BIN" "$DATADIR"
