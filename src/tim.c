/* See LICENSE file for copyright and license details. */

#include <stddef.h>

#include <sys/types.h>

#include <libgte.h>
#include <libgpu.h>

#include "tim.h"
#include "utils.h"

void dw_tim_load(u_long *addr)
{
	TIM_IMAGE image;

	DW_DEBUG_ASSERT(!OpenTIM(addr));
	DW_DEBUG_ASSERT(ReadTIM(&image) != NULL);

	if (image.paddr != NULL) {
		LoadImage(image.prect, image.paddr);
	}

	if (image.caddr != NULL) {
		LoadImage(image.crect, image.caddr);
	}
}

void dw_tim_load_rect(u_long *addr, RECT *prect, RECT *crect)
{
	TIM_IMAGE image;

	DW_DEBUG_ASSERT(!OpenTIM(addr));
	DW_DEBUG_ASSERT(ReadTIM(&image) != NULL);

	if (image.paddr != NULL) {
		LoadImage(prect, image.paddr);
	}

	if (image.caddr != NULL) {
		LoadImage(crect, image.caddr);
	}
}
