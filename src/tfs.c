/* See LICENSE file for copyright and license details. */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>

#include <libgte.h>
#include <libgpu.h>

#include "alloc.h"
#include "cd.h"
#include "map.h"
#include "tfs.h"
#include "utils.h"

dw_tfs *dw_tfs_init(uint8_t map_id)
{
	dw_tfs *tfs = xcalloc(1, sizeof(dw_tfs));
	dw_map_entry *entry = dw_get_map_entry(map_id);
	char name[256] = {0};
	sprintf(name, "\\MAP\\MAP%d\\%s.TFS;1", (map_id / 15) + 1, entry->name);
	printf("Loading tfs %s\n", name);

	size_t size;
	uint8_t *tfs_data = cd_read_alloc(name, &size);
	tfs->tfs_data = tfs_data;
	dw_tfs_config *config = (dw_tfs_config *)tfs_data;
	tfs->config = config;
	tfs->images = (dw_tfs_image *)&config->palettes[config->num_palettes];
	tfs->num_images = (size - 8 - config->num_palettes * 512) / sizeof(dw_tfs_image);
	/* DW_DEBUG_ASSERT(tfs->num_images == (config->width * config->height)); */

	for (uint32_t i = 0; i < config->num_palettes; ++i) {
		RECT rect = {
			.x = 0,
			.y = i + 481,
			.w = sizeof(dw_tfs_palette),
			.h = 1,
		};
		LoadImage(&rect, (u_long *)&config->palettes[i]);
	}

	return tfs;
}

void dw_tfs_deinit(dw_tfs *tfs)
{
	xfree(tfs->tfs_data);
	xfree(tfs);
}
