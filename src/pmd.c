/* See LICENSE file for copyright and license details. */

#include <stdint.h>
#include <stdlib.h>

#include <sys/types.h>

#include <gtemac.h>
#include <libgte.h>
#include <libgpu.h>

#include "inline_n.h"
#include "pmd.h"
#include "utils.h"

#define pmd_rot_average_nclip3(type, pkt, poly, flg, otz, backc)	\
	gte_ldv3c(&(pkt)->v1);						\
	gte_rtpt();							\
	gte_stflg(&(flg));						\
	if ((flg) & 0x80000000) {					\
		continue;						\
	}								\
	gte_nclip();							\
	if (!(backc)) {							\
		gte_stopz(&(otz));					\
		if ((otz) <= 0) {					\
			continue;					\
		}							\
	}								\
	gte_stsxy3_ ## type((poly));					\
	gte_avsz3();							\
	gte_stotz(&(otz));						\

#define pmd_rot_average_nclip4(type, pkt, poly, flg, otz, backc)	\
	gte_ldv3c(&(pkt)->v1);						\
	gte_rtpt();							\
	gte_stflg(&(flg));						\
	if ((flg) & 0x80000000) {					\
		continue;						\
	}								\
	gte_nclip();							\
	if (!(backc)) {							\
		gte_stopz(&(otz));					\
		if ((otz) <= 0) {					\
			continue;					\
		}							\
	}								\
	gte_stsxy3_ ## type(poly);					\
	gte_ldv0(&(pkt)->v4);						\
	gte_rtps();							\
	gte_stflg(&flg);						\
	if ((flg) & 0x80000000) {					\
		continue;						\
	}								\
	gte_stsxy(&poly->x3);						\
	gte_avsz4();							\
	gte_stotz(&(otz));						\

#define pmd_normal_color_col(type, poly, normals, colors)		\
	gte_ldv0(normals);						\
	gte_ldrgb(colors);						\
	gte_nccs();							\
	gte_strgb(&(poly)->r0);

#define pmd_normal_color_col3(type, poly, normals, colors)		\
	gte_ldv3c((normals));						\
	gte_ldrgb((colors));						\
	gte_ncct();							\
	gte_strgb3_ ## type((poly));					\

#define pmd_normal_color_col4(type, poly, normals, colors)		\
	pmd_normal_color_col3(type, poly, normals, colors)		\
	gte_ldv0(&((SVECTOR *)(normals))[3]);				\
	gte_nccs();							\
	gte_strgb(&(poly)->r3);						\

static void pmd_poly_ft3_sort(uint32_t *primitive, u_long *ot, int otlen,
			      int id)
{
	uint16_t npacket;
	pmd_poly_ft3 *pft3;
	POLY_FT3 *ft3;
	SVECTOR *normals;
	CVECTOR color;
	CVECTOR *colors;
	u_char code;
	int backc;
	int light;
	long flg;
	long otz;
	uint16_t i;

	npacket = PMD_NPACKET(primitive[0]);
	pft3 = (pmd_poly_ft3 *)&primitive[1];
	normals = (SVECTOR *)&pft3[npacket];
	color = (CVECTOR){ 128, 128, 128, 0 };
	colors = &color;

	backc = !!(PMD_TYPE(primitive[0]) & PMD_TYPE_NO_BACK_CLIP);
	light = !!(PMD_TYPE(primitive[0]) & PMD_TYPE_LIGHT);

	for (i = 0; i < npacket; ++i) {
		ft3 = &pft3[i].pkt[id];

		pmd_rot_average_nclip3(ft3, &pft3[i], ft3, flg, otz, backc);

		if (light) {
			code = ft3->code;
			pmd_normal_color_col(ft3, ft3,
					     &normals[i],
					     &colors[0]);
			ft3->code = code;
		}

		addPrim(&ot[otz >> (14 - otlen)], ft3);
	}
}

static void pmd_poly_ft4_sort(uint32_t *primitive, u_long *ot, int otlen,
			      int id)
{
	uint16_t npacket;
	pmd_poly_ft4 *pft4;
	POLY_FT4 *ft4;
	SVECTOR *normals;
	CVECTOR color;
	CVECTOR *colors;
	u_char code;
	int backc;
	int light;
	long flg;
	long otz;
	uint16_t i;

	npacket = PMD_NPACKET(primitive[0]);
	pft4 = (pmd_poly_ft4 *)&primitive[1];
	normals = (SVECTOR *)&pft4[npacket];
	color = (CVECTOR){ 128, 128, 128, 0 };
	colors = &color;

	backc = !!(PMD_TYPE(primitive[0]) & PMD_TYPE_NO_BACK_CLIP);
	light = !!(PMD_TYPE(primitive[0]) & PMD_TYPE_LIGHT);

	for (i = 0; i < npacket; ++i) {
		ft4 = &pft4[i].pkt[id];

		pmd_rot_average_nclip4(ft4, &pft4[i], ft4, flg, otz, backc);

		if (light) {
			code = ft4->code;
			pmd_normal_color_col(ft4, ft4,
					     &normals[i],
					     &colors[0]);
			ft4->code = code;
		}

		addPrim(&ot[otz >> (14 - otlen)], ft4);
	}
}

static void pmd_poly_gt3_sort(uint32_t *primitive, u_long *ot, int otlen,
			      int id)
{
	uint16_t npacket;
	pmd_poly_gt3 *pgt3;
	POLY_GT3 *gt3;
	SVECTOR *normals;
	CVECTOR color;
	CVECTOR *colors;
	u_char code;
	int backc;
	int light;
	long flg;
	long otz;
	uint16_t i;

	npacket = PMD_NPACKET(primitive[0]);
	pgt3 = (pmd_poly_gt3 *)&primitive[1];
	normals = (SVECTOR *)&pgt3[npacket];
	color = (CVECTOR){ 128, 128, 128, 0 };
	colors = &color;

	backc = !!(PMD_TYPE(primitive[0]) & PMD_TYPE_NO_BACK_CLIP);
	light = !!(PMD_TYPE(primitive[0]) & PMD_TYPE_LIGHT);

	for (i = 0; i < npacket; ++i) {
		gt3 = &pgt3[i].pkt[id];

		pmd_rot_average_nclip3(gt3, &pgt3[i], gt3, flg, otz, backc);

		if (light) {
			code = gt3->code;
			pmd_normal_color_col3(gt3, gt3,
					      &normals[i * 3],
					      &colors[0]);
			gt3->code = code;
		}

		addPrim(&ot[otz >> (14 - otlen)], gt3);
	}
}

static void pmd_poly_gt4_sort(uint32_t *primitive, u_long *ot, int otlen,
			      int id)
{
	uint16_t npacket;
	pmd_poly_gt4 *pgt4;
	POLY_GT4 *gt4;
	SVECTOR *normals;
	CVECTOR color;
	CVECTOR *colors;
	u_char code;
	int backc;
	int light;
	long flg;
	long otz;
	uint16_t i;

	npacket = PMD_NPACKET(primitive[0]);
	pgt4 = (pmd_poly_gt4 *)&primitive[1];
	normals = (SVECTOR *)&pgt4[npacket];
	color = (CVECTOR){ 128, 128, 128, 0 };
	colors = &color;

	backc = !!(PMD_TYPE(primitive[0]) & PMD_TYPE_NO_BACK_CLIP);
	light = !!(PMD_TYPE(primitive[0]) & PMD_TYPE_LIGHT);

	for (i = 0; i < npacket; ++i) {
		gt4 = &pgt4[i].pkt[id];

		pmd_rot_average_nclip4(gt4, &pgt4[i], gt4, flg, otz, backc);

		if (light) {
			code = gt4->code;
			pmd_normal_color_col4(gt4, gt4,
					      &normals[i * 4],
					      &colors[0]);
			gt4->code = code;
		}

		addPrim(&ot[otz >> (14 - otlen)], gt4);
	}
}

static void pmd_poly_f3_sort(uint32_t *primitive, u_long *ot, int otlen,
			     int id)
{
	uint16_t npacket;
	pmd_poly_f3 *pf3;
	POLY_F3 *f3;
	SVECTOR *normals;
	CVECTOR *colors;
	u_char code;
	int backc;
	int light;
	long flg;
	long otz;
	uint16_t i;

	npacket = PMD_NPACKET(primitive[0]);
	pf3 = (pmd_poly_f3 *)&primitive[1];
	normals = (SVECTOR *)&pf3[npacket];
	colors = (CVECTOR *)&normals[npacket];

	backc = !!(PMD_TYPE(primitive[0]) & PMD_TYPE_NO_BACK_CLIP);
	light = !!(PMD_TYPE(primitive[0]) & PMD_TYPE_LIGHT);

	for (i = 0; i < npacket; ++i) {
		f3 = &pf3[i].pkt[id];

		pmd_rot_average_nclip3(f3, &pf3[i], f3, flg, otz, backc);

		if (light) {
			code = f3->code;
			pmd_normal_color_col(f3, f3,
					     &normals[i],
					     &colors[i]);
			f3->code = code;
		}

		addPrim(&ot[otz >> (14 - otlen)], f3);
	}
}

static void pmd_poly_f4_sort(uint32_t *primitive, u_long *ot, int otlen,
			     int id)
{
	uint16_t npacket;
	pmd_poly_f4 *pf4;
	POLY_F4 *f4;
	SVECTOR *normals;
	CVECTOR *colors;
	u_char code;
	int backc;
	int light;
	long flg;
	long otz;
	uint16_t i;

	npacket = PMD_NPACKET(primitive[0]);
	pf4 = (pmd_poly_f4 *)&primitive[1];
	normals = (SVECTOR *)&pf4[npacket];
	colors = (CVECTOR *)&normals[npacket];

	backc = !!(PMD_TYPE(primitive[0]) & PMD_TYPE_NO_BACK_CLIP);
	light = !!(PMD_TYPE(primitive[0]) & PMD_TYPE_LIGHT);

	for (i = 0; i < npacket; ++i) {
		f4 = &pf4[i].pkt[id];

		pmd_rot_average_nclip4(f4, &pf4[i], f4, flg, otz, backc);

		if (light) {
			code = f4->code;
			pmd_normal_color_col(f4, f4,
					     &normals[i],
					     &colors[i]);
			f4->code = code;
		}

		addPrim(&ot[otz >> (14 - otlen)], f4);
	}
}

static void pmd_poly_g3_sort(uint32_t *primitive, u_long *ot, int otlen,
			     int id)
{
	uint16_t npacket;
	pmd_poly_g3 *pg3;
	POLY_G3 *g3;
	SVECTOR *normals;
	CVECTOR *colors;
	u_char code;
	int backc;
	int light;
	long flg;
	long otz;
	uint16_t i;

	npacket = PMD_NPACKET(primitive[0]);
	pg3 = (pmd_poly_g3 *)&primitive[1];
	normals = (SVECTOR *)&pg3[npacket];
	colors = (CVECTOR *)&normals[npacket];

	backc = !!(PMD_TYPE(primitive[0]) & PMD_TYPE_NO_BACK_CLIP);
	light = !!(PMD_TYPE(primitive[0]) & PMD_TYPE_LIGHT);

	for (i = 0; i < npacket; ++i) {
		g3 = &pg3[i].pkt[id];

		pmd_rot_average_nclip3(g3, &pg3[i], g3, flg, otz, backc);

		if (light) {
			code = g3->code;
			pmd_normal_color_col3(g3, g3,
					      &normals[i * 3],
					      &colors[i]);
			g3->code = code;
		}

		addPrim(&ot[otz >> (14 - otlen)], g3);
	}
}

static void pmd_poly_g4_sort(uint32_t *primitive, u_long *ot, int otlen,
			     int id)
{
	uint16_t npacket;
	pmd_poly_g4 *pg4;
	POLY_G4 *g4;
	SVECTOR *normals;
	CVECTOR *colors;
	u_char code;
	int backc;
	int light;
	long flg;
	long otz;
	uint16_t i;

	npacket = PMD_NPACKET(primitive[0]);
	pg4 = (pmd_poly_g4 *)&primitive[1];
	normals = (SVECTOR *)&pg4[npacket];
	colors = (CVECTOR *)&normals[npacket];

	backc = !!(PMD_TYPE(primitive[0]) & PMD_TYPE_NO_BACK_CLIP);
	light = !!(PMD_TYPE(primitive[0]) & PMD_TYPE_LIGHT);

	for (i = 0; i < npacket; ++i) {
		g4 = &pg4[i].pkt[id];

		pmd_rot_average_nclip4(g4, &pg4[i], g4, flg, otz, backc);

		if (light) {
			code = g4->code;
			pmd_normal_color_col4(g4, g4,
					      &normals[i * 4],
					      &colors[i]);
			g4->code = code;
		}

		addPrim(&ot[otz >> (14 - otlen)], g4);
	}
}

typedef void (*pmd_primitive_sort_func)(uint32_t *, u_long *, int, int);

static pmd_primitive_sort_func pmd_primitive_sort_jt[PMD_NUM_POLY_TYPES] = {
	pmd_poly_ft3_sort, pmd_poly_ft4_sort,
	pmd_poly_gt3_sort, pmd_poly_gt4_sort,
	pmd_poly_f3_sort, pmd_poly_f4_sort,
	pmd_poly_g3_sort, pmd_poly_g4_sort,
};

static void pmd_primitive_sort(uint32_t *primitive, u_long *ot, int otlen,
			       int id)
{
	uint16_t pmd_poly_type = PMD_POLY_TYPE(primitive[0]);

	pmd_primitive_sort_func func;
	func = pmd_primitive_sort_jt[pmd_poly_type];
	func(primitive, ot, otlen, id);
}

void pmd_init(pmd_model *pmd)
{
	uint32_t *ptr;
	uint32_t nptr;
	uint32_t i;
	uint32_t j;

	ptr = &pmd->object0;

	for (i = 0; i < pmd->nobj; ++i) {
		nptr = *ptr;
		++ptr;

		for (j = 0; j < nptr; ++j) {
			ptr[j] += (uint32_t)((uint8_t *)pmd);
		}

		ptr += nptr;
	}
}

pmd_object *pmd_get_object(pmd_model *pmd, uint32_t obj_no)
{
	pmd_object *object;
	uint32_t *ptr;
	uint32_t nptr;
	uint32_t i;

	object = NULL;
	ptr = &pmd->object0;

	for (i = 0; i < pmd->nobj; ++i) {
		if (i == obj_no) {
			object = (pmd_object *)ptr;
			break;
		}

		nptr = *ptr;
		ptr += nptr + 1;
	}

	return object;
}

#define get_tpage_xy(tpage, x, y)	\
	(((tpage) & ~0x81f) |		\
	 (((x) >> 6) & 0xf) |		\
	 (((y) >> 4) & 0x10) |		\
	 (((y) << 2) & 0x800))

#define set_tpage_xy(p, x, y)		\
	((p)->tpage = get_tpage_xy((p)->tpage, (x), (y)))

#define get_clut_x(p)			\
	((p)->clut & 0x3f)

#define get_clut_y(p)			\
	((p)->clut >> 6)

static void pmd_primitive_set_texture(uint32_t *primitive,
				      uint16_t tx, uint16_t ty,
				      int16_t cdx, int16_t cdy)
{
	uint16_t pmd_npacket = PMD_NPACKET(primitive[0]);
	uint16_t pmd_poly_type = PMD_POLY_TYPE(primitive[0]);
	uint32_t i;
	uint32_t j;

	switch (pmd_poly_type) {
	case PMD_POLY_TYPE_FT3:
		{
			pmd_poly_ft3 *pft3 = (pmd_poly_ft3 *)&primitive[1];
			for (i = 0; i < pmd_npacket; ++i) {
				for (j = 0; j < 2; ++j) {
					POLY_FT3 *ft3;
					ft3 = (POLY_FT3 *)&pft3[i].pkt[j];
					set_tpage_xy(ft3, tx, ty);
					setClut(ft3,
						get_clut_x(ft3) + cdx,
						get_clut_y(ft3) + cdy);
					if (ty % 256 >= 128) {
						ft3->v0 += 128;
						ft3->v1 += 128;
						ft3->v2 += 128;
					}
				}
			}
		}
		break;
	case PMD_POLY_TYPE_FT4:
		{
			pmd_poly_ft4 *pft4 = (pmd_poly_ft4 *)&primitive[1];
			for (i = 0; i < pmd_npacket; ++i) {
				for (j = 0; j < 2; ++j) {
					POLY_FT4 *ft4;
					ft4 = (POLY_FT4 *)&pft4[i].pkt[j];
					set_tpage_xy(ft4, tx, ty);
					setClut(ft4,
						get_clut_x(ft4) + cdx,
						get_clut_y(ft4) + cdy);
					if (ty % 256 >= 128) {
						ft4->v0 += 128;
						ft4->v1 += 128;
						ft4->v2 += 128;
						ft4->v3 += 128;
					}
				}
			}
		}
		break;
	case PMD_POLY_TYPE_GT3:
		{
			pmd_poly_gt3 *pgt3 = (pmd_poly_gt3 *)&primitive[1];
			for (i = 0; i < pmd_npacket; ++i) {
				for (j = 0; j < 2; ++j) {
					POLY_GT3 *gt3;
					gt3 = (POLY_GT3 *)&pgt3[i].pkt[j];
					set_tpage_xy(gt3, tx, ty);
					setClut(gt3,
						get_clut_x(gt3) + cdx,
						get_clut_y(gt3) + cdy);
					if (ty % 256 >= 128) {
						gt3->v0 += 128;
						gt3->v1 += 128;
						gt3->v2 += 128;
					}
				}
			}
		}
		break;
	case PMD_POLY_TYPE_GT4:
		{
			pmd_poly_gt4 *pgt4 = (pmd_poly_gt4 *)&primitive[1];
			for (i = 0; i < pmd_npacket; ++i) {
				for (j = 0; j < 2; ++j) {
					POLY_GT4 *gt4;
					gt4 = (POLY_GT4 *)&pgt4[i].pkt[j];
					set_tpage_xy(gt4, tx, ty);
					setClut(gt4,
						get_clut_x(gt4) + cdx,
						get_clut_y(gt4) + cdy);
					if (ty % 256 >= 128) {
						gt4->v0 += 128;
						gt4->v1 += 128;
						gt4->v2 += 128;
						gt4->v3 += 128;
					}
				}
			}
		}
		break;
	default:
		break;
	}
}

void pmd_object_set_texture(pmd_object *object,
			    uint16_t tx, uint16_t ty,
			    uint16_t cdx, uint16_t cdy)
{
	uint32_t *primitive;
	uint32_t i;

	for (i = 0; i < object->nptr; ++i) {
		primitive = object->ptr[i];
		pmd_primitive_set_texture(primitive, tx, ty, cdx, cdy);
	}
}

void pmd_object_sort(pmd_object *object, u_long *ot, int otlen, int id)
{
	uint32_t i;

	for (i = 0; i < object->nptr; ++i) {
		pmd_primitive_sort(object->ptr[i], ot, otlen, id);
	}
}
