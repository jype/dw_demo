/* See LICENSE file for copyright and license details. */

#include <stdlib.h>

#include "alloc.h"
#include "utils.h"

#ifdef DW_DEBUG_ALLOC
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

typedef struct {
	const char *file;
	int line;
	void *ptr;
	bool tracked;
} alloc_info;

static alloc_info allocs[DW_MAX_ALLOCS];
static size_t num_allocs;

static void register_alloc(void *ptr, const char *file, int line)
{
	DW_DEBUG_ASSERT(num_allocs < DW_MAX_ALLOCS);

	allocs[num_allocs].file = file;
	allocs[num_allocs].line = line;
	allocs[num_allocs].ptr = ptr;
	allocs[num_allocs].tracked = true;
	++num_allocs;
}

static void unregister_alloc(void *ptr)
{
	size_t i;

	DW_DEBUG_ASSERT(num_allocs);

	for (i = 0; i < num_allocs; ++i) {
		if (allocs[i].ptr == ptr) {
			break;
		}
	}

	if (i == num_allocs) {
		printf("%s: invalid free %08x\n", __func__, (uint32_t)ptr);
	} else {
		memmove((void *)&allocs[i], (void *)&allocs[i + 1],
			(DW_MAX_ALLOCS - i) * sizeof(*allocs));
		--num_allocs;
	}
}

void *debug_malloc(size_t size, const char *file, int line)
{
	void *ptr = malloc3(size);
	DW_DEBUG_ASSERT(ptr != NULL);

	register_alloc(ptr, file, line);

	return ptr;
}

void *debug_calloc(size_t nmemb, size_t size, const char *file, int line)
{
	void *ptr = calloc3(nmemb, size);
	DW_DEBUG_ASSERT(ptr != NULL);

	register_alloc(ptr, file, line);

	return ptr;
}

void *debug_realloc(void *ptr, size_t size, const char *file, int line)
{
	ptr = realloc3(ptr, size);
	DW_DEBUG_ASSERT(ptr != NULL);

	register_alloc(ptr, file, line);

	return ptr;
}

void debug_free(void *ptr, const char *file, int line)
{
	unregister_alloc(ptr);

	free3(ptr);
}

void reset_allocs(void)
{
	size_t i;

	for (i = 0; i < num_allocs; ++i) {
		allocs[i].tracked = false;
	}
}

void check_allocs(void)
{
	size_t i;

	for (i = 0; i < num_allocs; ++i) {
		if (allocs[i].tracked && (allocs[i].ptr != NULL)) {
			printf("%s: leaked memory %08x from %s:%d\n",
			       __func__, (uint32_t)allocs[i].ptr,
			       allocs[i].file, allocs[i].line);
		}
	}
}
#else
void *xmalloc(size_t size)
{
	void *ptr = malloc3(size);
	DW_DEBUG_ASSERT(ptr != NULL);

	return ptr;
}

void *xcalloc(size_t nmemb, size_t size)
{
	void *ptr = calloc3(nmemb, size);
	DW_DEBUG_ASSERT(ptr != NULL);

	return ptr;
}

void *xrealloc(void *ptr, size_t size)
{
	ptr = realloc3(ptr, size);
	DW_DEBUG_ASSERT(ptr != NULL);

	return ptr;
}

void xfree(void *ptr)
{
	free3(ptr);
}
#endif
