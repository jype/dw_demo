/* See LICENSE file for copyright and license details. */

#include <sys/types.h>

#include <libgte.h>

#include "graphics.h"

const MATRIX dw_identity_matrix = {
	.m = {
		{ ONE, 0, 0 },
		{ 0, ONE, 0 },
		{ 0, 0, ONE },
	},
	.t = { 0, 0, 0 },
};

MATRIX dw_world_screen_matrix;
MATRIX dw_light_source_matrix;
MATRIX dw_light_color_matrix;
MATRIX dw_light_matrix;
