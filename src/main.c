/* See LICENSE file for copyright and license details. */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>

#include <libapi.h>
#include <libds.h>
#include <libetc.h>
#include <libgte.h>
#include <libgpu.h>
#include <libsnd.h>
#include <libspu.h>

#include "alloc.h"
#include "bin.h"
#include "cd.h"
#include "graphics.h"
#include "tim.h"
#include "utils.h"

#define SCR_Z			1024
#define OTLENGTH		3
#define OTSIZE			(1 << (OTLENGTH))

#define MVOL 127
#define SVOL 0x50

static char seq_table[SS_SEQ_TABSIZ * 1 * 1];  /* seq table size */

extern void test_map(void);
extern void test_mtn(void);

static int pad_read(void);

extern unsigned long __bss_end__;
unsigned long _heapbase = (unsigned long)&__bss_end__;

#define FNT_MAX_CHARS		256

static char fnt_buf[FNT_MAX_CHARS];

static SPRT fnt_text[2];
static SPRT fnt_shadow[2];
static DR_TPAGE fnt_text_tpage[2];

static int16_t fnt_putc(char c, uint16_t offset_x, uint16_t offset_y)
{
	uint8_t buf[6 * 11] = {0};
	uint16_t *bit_ptr = (uint16_t *)&font_start[c * 24];
	uint16_t bits;
	uint8_t pixels;

	for (int i = 0; i < 11; ++i) {
		bits = bit_ptr[i];

		for (int j = 0; j < 6; ++j, bits <<= 2) {
			pixels = 0;

			if (!(bits & 0x8000)) {
				pixels = 0x1;
			}
			if (!(bits & 0x4000)) {
				pixels |= 0x10;
			}

			buf[i * 6 + j] = pixels;
		}
	}

	RECT rect = {
		.x = (offset_x >> 2) + 512,
		.y = offset_y + 256,
		.w = 3,
		.h = 11,
	};
	LoadImage(&rect, (u_long *)buf);

	return bit_ptr[11];
}

static void fnt_flush(int16_t x, int16_t y)
{
	int16_t offset_x = x;
	int16_t offset_y = y;
	size_t i;
	char c;

	RECT rect = {
		.x = 512,
		.y = 256,
		.w = 256,
		.h = 128,
	};

	ClearImage(&rect, 0, 0, 0);

	for (i = 0; i < FNT_MAX_CHARS; ++i) {
		c = fnt_buf[i];
		if (!c) {
			break;
		} else if (c == '\n') {
			offset_x = x;
			offset_y += 12;
		} else if (c >= ' ' || (c >= -53 && c <= -50)) {
			if (c == ' ') {
				c = 62;
			} else if (c == '\\') {
				c = 69;
			} else if (c >= -53 && c <= -50) {
				c = c + 128;
			} else if (c >= '0' && c <= '9') {
				c = c - '0' + 52;
			}
			else if (c >= 'A' && c <= 'Z') {
				c = c - 'A';
			} else if (c >= 'a' && c <= 'z') {
				c = c - 'a' + 26;
			} else {
				goto skip;
			}

			offset_x += fnt_putc(c, offset_x, offset_y);
		}
skip:
		/* printf("skip char '%d'\n", c); */
		continue;
	}
}

typedef struct {
	TILE bg;
	LINE_F2 lines[14];
	POLY_FT4 corners[4];
	POLY_FT4 button;
} dw_ui_box;

static void dw_ui_box_init(dw_ui_box *box)
{
	uint32_t ofs_x = 160;
	uint32_t ofs_y = 120;

	/* Box */
	uint32_t Rect254x56Semi[] = {
		0x62000000, 0x002DFF82, 0x003800FE,
	};

	setTile(&box->bg);
	memcpy(&box->bg.r0, Rect254x56Semi, sizeof(Rect254x56Semi));
	box->bg.x0 += ofs_x;
	box->bg.y0 += ofs_y;
	/* dump_TILE(&box->bg); */

	/* Box outline */
	uint32_t Lines[14][3] = {
		{ 0x40020202, 0x002D0080, 0x00620080, },
		{ 0x40020202, 0x002DFF81, 0x0062FF81, },
		{ 0x40020202, 0x002E0083, 0x00620083, },
		{ 0x40C59F4A, 0x002E0082, 0x00620082, },
		{ 0x40FAD990, 0x002E0081, 0x00620081, },
		{ 0x40FAD990, 0x002EFF80, 0x0062FF80, },
		{ 0x40C59F4A, 0x002EFF7F, 0x0062FF7F, },
		{ 0x40020202, 0x002EFF7E, 0x0062FF7E, },
		{ 0x40020202, 0x0064FF82, 0x00640080, },
		{ 0x40FAD990, 0x0063FF82, 0x00630080, },
		{ 0x40020202, 0x0062FF82, 0x00620080, },
		{ 0x40020202, 0x002CFF82, 0x002C0080, },
		{ 0x40FAD990, 0x002BFF82, 0x002B0080, },
		{ 0x40020202, 0x002AFF82, 0x002A0080, },
	};

	for (size_t i = 0; i < DW_ARRAY_SIZE(box->lines); ++i) {
		setLineF2(&box->lines[i]);
		memcpy(&box->lines[i].r0, &Lines[i][0], 3 * sizeof(uint32_t));
		box->lines[i].x0 += ofs_x;
		box->lines[i].y0 += ofs_y;
		box->lines[i].x1 += ofs_x;
		box->lines[i].y1 += ofs_y;
		/* dump_LINE_F2(&box->lines[i]); */
	}

	/* Box corners */
	uint32_t Corners[4][9] = {
		{
			0x2C808080, 0x00610080, 0x7B06947C, 0x00610084,
			0x00059480, 0x00650080, 0x002B987C, 0x00650084,
			0x40029880,
		},
		{
			0x2C808080, 0x0061FF7E, 0x7B069478, 0x0061FF82,
			0x0005947C, 0x0065FF7E, 0x3C719878, 0x0065FF82,
			0x7A80987C,
		},
		{
			0x2C808080, 0x002A0080, 0x7B06907C, 0x002A0084,
			0x00059080, 0x002E0080, 0x3C97947C, 0x002E0084,
			0x00159480,
		},
		{
			0x2C808080, 0x002AFF7E, 0x7B069078, 0x002AFF82,
			0x0005907C, 0x002EFF7E, 0x3C409478, 0x002EFF82,
			0x0000947C,
		},
	};

	for (size_t i = 0; i < DW_ARRAY_SIZE(box->corners); ++i) {
		setPolyFT4(&box->corners[i]);
		memcpy(&box->corners[i].r0, &Corners[i][0], 9 * sizeof(uint32_t));
		box->corners[i].x0 += ofs_x;
		box->corners[i].y0 += ofs_y;
		box->corners[i].x1 += ofs_x;
		box->corners[i].y1 += ofs_y;
		box->corners[i].x2 += ofs_x;
		box->corners[i].y2 += ofs_y;
		box->corners[i].x3 += ofs_x;
		box->corners[i].y3 += ofs_y;
	}

	/* Button */
	uint32_t QuadSemiTex[] = {
		0x2E808080, 0x0055006D, 0x7D06B75C, 0x0055007D,
		0x0025B76C, 0x0061006D, 0x0000C35C, 0x0061007D,
		0x8015C36C,
	};

	setPolyFT4(&box->button);
	memcpy(&box->button.r0, &QuadSemiTex[0], 9 * sizeof(uint32_t));
	box->button.x0 += ofs_x;
	box->button.y0 += ofs_y;
	box->button.x1 += ofs_x;
	box->button.y1 += ofs_y;
	box->button.x2 += ofs_x;
	box->button.y2 += ofs_y;
	box->button.x3 += ofs_x;
	box->button.y3 += ofs_y;

	/* Text and text shadows */
	POLY_FT4 testp;

	uint32_t QuadTexText0[] = {
		0x2C000000, 0x0056FF84, 0x7A0D5400, 0x00560080,
		0x001B54FC, 0x0062FF84, 0x0C156000, 0x00620080,
		0x008460FC,
	};

	memcpy(&testp.r0, &QuadTexText0[0], 9 * sizeof(uint32_t));
	/* printf("text shadow poly\n"); */
	/* dump_POLY_FT4(&testp); */

	uint32_t QuadTexText1[] = {
		0x2C808080, 0x0055FF83, 0x7A0D5400, 0x0055007F,
		0x001B54FC, 0x0061FF83, 0x7A006000, 0x0061007F,
		0x007760FC,
	};

	memcpy(&testp.r0, &QuadTexText1[0], 9 * sizeof(uint32_t));
	/* printf("text poly\n"); */
	/* dump_POLY_FT4(&testp); */

	/* uint32_t QuadTexText2[] = { */
	/* 	0x2C000000, 0x0049FF84, 0x7A0D4800, 0x00490080, */
	/* 	0x001B48FC, 0x0055FF84, 0x00155400, 0x00550080, */
	/* 	0x007754FC, */
	/* }; */

	/* uint32_t QuadTexText3[] = { */
	/* 	0x2C808080, 0x0048FF83, 0x7A0D4800, 0x0048007F, */
	/* 	0x001B48FC, 0x0054FF83, 0x00005400, 0x0054007F, */
	/* 	0x007854FC, */
	/* }; */

	/* uint32_t QuadTexText4[] = { */
	/* 	0x2C000000, 0x003CFF84, 0x7A0D3C00, 0x003C0080, */
	/* 	0x001B3CFC, 0x0048FF84, 0x00004800, 0x00480080, */
	/* 	0x3C9748FC, */
	/* }; */

	/* uint32_t QuadTexText5[] = { */
	/* 	0x2C808080, 0x003BFF83, 0x7A0D3C00, 0x003B007F, */
	/* 	0x001B3CFC, 0x0047FF83, 0x007B4800, 0x0047007F, */
	/* 	0x3C9748FC, */
	/* }; */

	/* uint32_t QuadTexText6[] = { */
	/* 	0x2C000000, 0x002FFF84, 0x7A0D3000, 0x002F0080, */
	/* 	0x001B30FC, 0x003BFF84, 0x007F3C00, 0x003B0080, */
	/* 	0x3C403CFC, */
	/* }; */

	/* uint32_t QuadTexText7[] = { */
	/* 	0x2C808080, 0x002EFF83, 0x7A0D3000, 0x002E007F, */
	/* 	0x001B30FC, 0x003AFF83, 0x48803C00, 0x003A007F, */
	/* 	0x00423CFC, */
	/* }; */
}

#define FAALL_ENTRY_SIZE	0x13800
#define FAALL_SECTOR_SIZE ((FAALL_ENTRY_SIZE) / (CD_SECTOR_SIZE))

#define FAALL_NUM_ENTRIES	33

__attribute__((aligned(8)))
static uint8_t buf[FAALL_ENTRY_SIZE];

static short vab = -1;

static uint32_t *seq_ptr;
static short num_seq;
static short seq = -1;

static void bgm_load(uint8_t bgm_id)
{
	cd_read_sectors("\\SOUND\\VHB\\FAALL.VHB;1", buf,
			bgm_id * FAALL_SECTOR_SIZE,
			FAALL_SECTOR_SIZE);

	uint32_t *ptr = (uint32_t *)buf;

	uint32_t vh_offset = *ptr++;
	DW_DEBUG_ASSERT(vh_offset == 0x10);

	uint32_t vb_offset = *ptr++;
	uint32_t seq_offset = *ptr++;

	vab = SsVabOpenHead(&buf[vh_offset], -1);
	DW_DEBUG_ASSERT(vab >= 0);

	DW_DEBUG_ASSERT(SsVabTransBody(&buf[vb_offset], vab) == vab);

	SsVabTransCompleted(SS_WAIT_COMPLETED);

	seq_ptr = (uint32_t *)&buf[seq_offset];
	num_seq = *seq_ptr / sizeof(uint32_t);
	DW_DEBUG_ASSERT(num_seq);
}

void bgm_unload(void)
{
	SsSeqStop(seq);
	SsSeqClose(seq);
	SsVabClose(vab);
}

static void bgm_play(uint8_t seq_id)
{
	uint32_t *ptr;
	uint32_t offset;

	DW_DEBUG_ASSERT(seq_id < num_seq);

	offset = seq_ptr[seq_id];
	ptr = (uint32_t *)&((uint8_t *)seq_ptr)[offset];

	seq = SsSeqOpen((u_long *)ptr, vab);
	SsSeqSetVol(seq, SVOL, SVOL);
	SsSeqPlay(seq, SSPLAY_PLAY, SSPLAY_INFINITY);
}

static void bgm_stop(void)
{
	SsSeqStop(seq);
	SsSeqClose(seq);
	SpuSetKey(SPU_OFF, SPU_ALLCH);
}

static int cur_bgm_id;
static int cur_seq_id;
static bool bgm_changed;
static bool seq_changed;

int
main(void)
{
	u_long ot[2][OTSIZE];
	int id = 0;

	db_init(320, 240, SCR_Z, 60, 120, 120);
	/* db_font_init(); */

	DsInit();

	SsInit();
	SsSetTableSize(seq_table, 1, 1);
	SsSetTickMode(SS_TICK240);

	SsStart();

	SsSetMVol(MVOL, MVOL);

	SpuReverbAttr reverb_attr;
	reverb_attr.mask = 7;
	reverb_attr.mode = 0x103;
	reverb_attr.depth.right = 0x7000;
	reverb_attr.depth.left = 0x7000;
	SpuSetReverbModeParam(&reverb_attr);
	SpuClearReverbWorkArea(3);
	SpuSetReverbDepth(&reverb_attr);
	SpuSetReverbVoice(1,0x7fffff);
	SpuSetReverb(1);

	cur_bgm_id = 27;
	cur_seq_id = 0;
	bgm_changed = true;
	seq_changed = true;

	dw_ui_box box[2];

	dw_ui_box_init(&box[0]);
	box[1] = box[0];

	u_long *tim;

	tim = cd_read_alloc("\\ETCDAT\\SYSTEM_W.TIM;1", NULL);
	dw_tim_load(tim);
	xfree(tim);

	RECT rect = {
		.x = 208,
		.y = 488,
		.w = 16,
		.h = 1,
	};
	LoadImage(&rect, (u_long *)&font_clut_start);

	setSprt(&fnt_text[0]);
	setRGB0(&fnt_text[0], 0x80, 0x80, 0x80);
	setXY0(&fnt_text[0], 40, 168);
	setWH(&fnt_text[0], 256, 128);
	setUV0(&fnt_text[0], 0, 0);
	fnt_text[0].clut = 0x7a0d;

	memcpy(&fnt_text[1], &fnt_text[0], sizeof(*fnt_text));

	memcpy(&fnt_shadow[0], &fnt_text[0], sizeof(*fnt_text));
	setRGB0(&fnt_shadow[0], 0, 0, 0);
	fnt_shadow[0].x0 += 1;
	fnt_shadow[0].y0 += 1;

	memcpy(&fnt_shadow[1], &fnt_shadow[0], sizeof(*fnt_text));

	SetDrawTPage(&fnt_text_tpage[0], 0, 1, getTPage(0, 0, 512, 256));
	SetDrawTPage(&fnt_text_tpage[1], 0, 1, getTPage(0, 0, 512, 256));

	while (true) {
		while (DrawSync(1));
		VSync(0);

		if (bgm_changed) {
			if (seq >= 0) {
				bgm_unload();
			}
			bgm_load(cur_bgm_id);
			cur_seq_id = 0;
			bgm_changed = false;
			seq_changed = true;
		}

		if (seq_changed) {
			if (seq >= 0) {
				bgm_stop();
			}
			bgm_play(cur_seq_id);
			seq_changed = false;
		}

		FntLoad(512 + 64, 256);

		SetDispMask(1);

		setRGB0(&dw_draw[0], 60, 120, 120);
		setRGB0(&dw_draw[1], 60, 120, 120);

		int frame = 0;

		u_short o1[2] = { 160, 120 };
		DR_OFFSET ofs[2];
		u_short o2[2] = { 160, 120+240 };
		SetDrawOffset(&ofs[0], o1);
		SetDrawOffset(&ofs[1], o2);

		sprintf(fnt_buf,
			"Sta\\Sel BGM %d\\%d\n"
			"L1\\R1    SEQ %d\\%d\n"
			"\xcb Map viewer\n"
			"\xcd Animation viewer\n",
			cur_bgm_id + 1, FAALL_NUM_ENTRIES,
			cur_seq_id + 1, num_seq);

		fnt_flush(0, 0);
		while (DrawSync(1));

		while (true) {
			int state = pad_read();
			if (state) {
				switch (state) {
				case 1:
					test_mtn();
					break;
				case 2:
					test_map();
					break;
				default:
					break;
				}
				break;

				fnt_flush(0, 0);
				while (DrawSync(1));
			}

			id = id ? 0 : 1;

			ClearOTagR(ot[id], OTSIZE);

			AddPrim(ot[id], &fnt_text[id]);
			AddPrim(ot[id], &fnt_shadow[id]);
			AddPrim(ot[id], &fnt_text_tpage[id]);

			int c = (frame % 36) / 9;
			int b = (c < 3) ? c : 1;
			setUVWH(&box[id].button, 76 + b * 16, 183, 16, 12);
			++frame;

			for (size_t i = 0; i < DW_ARRAY_SIZE(box[id].corners); ++i) {
				AddPrim(ot[id], &box[id].corners[i]);
			}

			for (size_t i = 0; i < DW_ARRAY_SIZE(box[id].lines); ++i) {
				AddPrim(ot[id], &box[id].lines[i]);
			}

			AddPrim(ot[id], &box[id].button);
			AddPrim(ot[id], &box[id].bg);

			db_swap(&ot[id][OTSIZE - 1], 0);
		}
	}

	return 0;
}

static int pad_read(void)
{
	static int fcount = 0;

	if (!PAD_DATA_VALID(&pad_port1) ||
	    ((PAD_GET_TYPE(&pad_port1) != STD_PAD) &&
	     (PAD_GET_TYPE(&pad_port1) != ANALOG_JOY))) {
		return 0;
	}

	if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_RDOWN)) return 1;
	if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_RRIGHT)) return 2;

	if (!fcount) {
		if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_SELECT)) {
			--cur_bgm_id;
			if (cur_bgm_id < 0) {
				cur_bgm_id = FAALL_NUM_ENTRIES - 1;
			}
			bgm_changed = true;
			fcount = 20;
			return 3;
		}

		if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_START)) {
			++cur_bgm_id;
			if (cur_bgm_id >= FAALL_NUM_ENTRIES) {
				cur_bgm_id = 0;
			}
			bgm_changed = true;
			fcount = 20;
			return 3;
		}

		if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_L1)) {
			--cur_seq_id;
			if (cur_seq_id < 0) {
				cur_seq_id = num_seq - 1;
			}
			seq_changed = true;
			fcount = 20;
			return 3;
		}

		if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_R1)) {
			++cur_seq_id;
			if (cur_seq_id >= num_seq) {
				cur_seq_id = 0;
			}
			seq_changed = true;
			fcount = 20;
			return 3;
		}
	} else {
		--fcount;
	}

	return 0;
}
