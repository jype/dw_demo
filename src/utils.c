/* See LICENSE file for copyright and license details. */

#include <stdio.h>

#include "utils.h"

void debug_assert_fail(const char *assertion, const char *file, int line)
{
	printf("%s:%d: Assertion '%s' failed.\n", file, line, assertion);
	while (1);
}
