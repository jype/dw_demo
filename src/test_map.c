/* See LICENSE file for copyright and license details. */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>

#include <libapi.h>
#include <libds.h>
#include <libetc.h>
#include <libgte.h>
#include <libgpu.h>

#include "alloc.h"
#include "bg.h"
#include "bin.h"
#include "cd.h"
#include "entity.h"
#include "graphics.h"
#include "inline_n.h"
#include "gs.h"
#include "map.h"
#include "pmd.h"
#include "skeleton.h"
#include "tfs.h"
#include "tim.h"
#include "quad.h"
#include "utils.h"

#define DW_OT_LAYER_MAP_FG_0	0
#define DW_OT_LAYER_MAP_FG_1	1
#define DW_OT_LAYER_MAP_FG_2	2
#define DW_OT_LAYER_MODEL	3
#define DW_OT_LAYER_MAP_BG_0	((OTSIZE) - 5)
#define DW_OT_LAYER_MAP_BG_1	((OTSIZE) - 4)
#define DW_OT_LAYER_MAP_BG_2	((OTSIZE) - 3)
#define DW_OT_LAYER_MAP_BG_3	((OTSIZE) - 2)
#define DW_OT_NUM_LAYERS	8

#define SCR_Z			1024
#define OTLENGTH		12
#define OTSIZE			((1 << (OTLENGTH)) + (DW_OT_NUM_LAYERS))

#include "params.h"
#include "skeleton.h"

static int pad_read(void);

static dw_entity *entities[8];
static VECTOR target_position;

static dw_bg *bg;
static bool bg_disabled;

static struct map_obj {
	SPRT sprt;
	DR_TPAGE tpage;
	dw_map_object *obj;
} objs[200];

static struct map_inst {
	SPRT sprt[2];
	DR_TPAGE tpage[2];
	dw_map_instance *inst;
	int x;
	int y;
	int frame;
	int counter;
	int wub;
	bool enabled;
} insts[200];

static int nobj;
static int ninst;

static bool objects_disabled;
static bool font_enabled = true;

static VECTOR vp;
static VECTOR vr;

static uint8_t cur_map_id = 0;
static int map_id_change;

static dw_map *map;
static dw_tfs *tfs;

static int cur_palette;

static void load_palette(int palette)
{
	RECT rect = {
		.x = 0,
		.y = 481,
		.w = sizeof(dw_tfs_palette),
		.h = 1,
	};

	palette = DW_CLAMP(palette, 0, tfs->config->num_palettes - 1);
	LoadImage(&rect, (u_long *)&tfs->config->palettes[palette]);
}

static void map_unload(void)
{
	if (map != NULL) {
		dw_map_deinit(map);
		map = NULL;
	}

	if (tfs != NULL) {
		dw_tfs_deinit(tfs);
		tfs = NULL;
	}

	memset(objs, 0, DW_ARRAY_SIZE(objs) * sizeof(*objs));
	memset(insts, 0, DW_ARRAY_SIZE(insts) * sizeof(*insts));

	for (size_t i = 0; i < DW_ARRAY_SIZE(entities); ++i) {
		if (entities[i] != NULL) {
			dw_entity_deinit(entities[i]);
			entities[i] = NULL;
		}
	}

	if (bg != NULL) {
		dw_bg_deinit(bg);
		bg = NULL;
	}
}

static int map_load(uint8_t map_id)
{
	switch (map_id) {
		case 50: /* YAKA01 missing */
		case 61: /* YAKA21 missing */
		case 65: /* YAKA25 duplicate */
		case 239:
		case 240:
		case 241:
		case 242:
		case 243:
		case 244:
		case 245:
		case 246:
		case 250:
		case 251:
		case 252:
		case 255:
			printf("Skipping map_id %d\n", map_id);
			return 1;
		default:
			break;
	}

	printf("Loading map_id %d\n", map_id);

	map = dw_map_init(map_id);
	tfs = dw_tfs_init(map_id);
	if ((uint32_t)cur_palette >= tfs->config->num_palettes) {
		cur_palette = 0;
	}
	load_palette(cur_palette);

	int slot = 0;

	entities[0] = dw_entity_init(0, slot++);
	entities[0]->position.vx = map->elements->spawn_x[0];
	entities[0]->position.vy = map->elements->spawn_y[0];
	entities[0]->position.vz = map->elements->spawn_z[0];
	entities[0]->rotation.vy = map->elements->spawn_rotation[0];

	entities[1] = dw_entity_init(3, slot++);
	entities[1]->position.vx = map->elements->spawn_x[1];
	entities[0]->position.vy = map->elements->spawn_y[0];
	entities[1]->position.vz = map->elements->spawn_z[1];
	entities[1]->rotation.vy = map->elements->spawn_rotation[1];

	for (int i = 2; i < map->elements->num_digimon && slot < 3; ++i) {
	/* for (int i = 1; i < 1; ++i) { */
		uint8_t id;
		do {
			srand(GetRCnt(0));
			id = (uint8_t)rand() % 180;
		} while ((id == 62) || (id == 114));
		entities[i] = dw_entity_init(id, slot++);
		entities[i]->position.vx = map->elements->spawn_x[i];
		entities[i]->position.vy = map->elements->spawn_y[i];
		entities[i]->position.vz = map->elements->spawn_z[i];
		entities[i]->rotation.vy = map->elements->spawn_rotation[i];
		/* entities[i]->position.vx = map->elements->digimons[i].position_x; */
		/* entities[i]->position.vy = map->elements->digimons[i].position_y; */
		/* entities[i]->position.vz = map->elements->digimons[i].position_z; */
		/* entities[i]->rotation.vx = map->elements->digimons[i].rotation_x; */
		/* entities[i]->rotation.vy = map->elements->digimons[i].rotation_y; */
		/* entities[i]->rotation.vz = map->elements->digimons[i].rotation_z; */
	}

	DW_DEBUG_ASSERT(map->elements != NULL);

	nobj = (map->objects != NULL) ? map->objects->num_objects : 0;
	ninst = (map->instances != NULL) ? map->instances->num_instances : 0;

	DW_DEBUG_ASSERT((size_t)nobj < DW_ARRAY_SIZE(objs));
	DW_DEBUG_ASSERT((size_t)ninst < DW_ARRAY_SIZE(insts));

	for (int i = 0; i < nobj; ++i) {
		dw_map_object *o = &map->objects->objects[i];
		objs[i].obj = o;
		setSprt(&objs[i].sprt);
		setRGB0(&objs[i].sprt, 0x80, 0x80, 0x80);
		int x, y;
		int tp;
		setWH(&objs[i].sprt, o->width, o->height);
		if (o->clut == 0xffff) {
			setUV0(&objs[i].sprt, o->uv_x&0x7f, o->uv_y);
			setClut(&objs[i].sprt, 0, 481);
			x = o->uv_x / 2 + 384;
			y = o->uv_y;
			tp = 1;
		} else if (o->clut < 16) {
			setUV0(&objs[i].sprt, o->uv_x, o->uv_y);
			setClut(&objs[i].sprt, o->clut * 16, 486);
			x = o->uv_x/4 + 384;
			y = o->uv_y;
			tp = 0;
		} else {
			setUV0(&objs[i].sprt, o->uv_x&0x7f, o->uv_y);
			setClut(&objs[i].sprt, 0, 468 + o->clut);
			x = o->uv_x / 2 + 384;
			y = o->uv_y;
			tp = 1;
		}

		SetSemiTrans(&objs[i].sprt, !!(o->transparency & 3));

		int tpage = getTPage(tp, o->transparency & 3, x, y);
		SetDrawTPage(&objs[i].tpage, 0, 1, tpage);
	}

	for (int i = 0; i < ninst; ++i) {
		dw_map_instance *inst = &map->instances->instances[i];
		insts[i].inst = inst;
		insts[i].enabled = true;
	}

	SetGeomScreen(map->config->distance);

	vp = (VECTOR){ map->config->vpx * 2, map->config->vpy * 2, map->config->vpz * 2, 0 };
	vr = (VECTOR){ map->config->vrx * 2, map->config->vry * 2, map->config->vrz * 2, 0 };

	target_position = vr;

	bg = dw_bg_init(map, tfs, 320, 240);

	return 0;
}

static dw_quad_ft4 screen_quad;

static uint8_t screen_fade;

static void screen_quad_init(void)
{
	dw_quad_ft4_init(&screen_quad, DW_PLANE_XY, 0, 0, 320, 240);

	pmd_poly_ft4 *pft4 = &screen_quad.pft4;

	POLY_FT4 *ft4 = &pft4->pkt[0];
	/* setRGB0(ft4, 0x30, 0x30, 0x30); */
	/* /1* setRGB0(ft4, 0xa0, 0xa0, 0xa0); *1/ */
	/* setUVWH(ft4, 96, 253, 159, 2); */
	/* setTPage(ft4, 1, 2, 864, 384); */
	/* /1* setClut(ft4, 0, 487); *1/ */
	/* ft4->clut = 0x7d90; */
	/* setSemiTrans(ft4, 1); */

	uint32_t dump[] = {
		0x2EA0A0A0,
		0xFF88FF60,
		0x7D90FDFA,
		0xFF8800A0,
		0x005EFDFC,
		0x0078FF60,
		0x3440FFFA,
		0x007800A0,
		0x0000FFFC,
	};

	uint32_t *ptr = (uint32_t *)&ft4->r0;
	for (size_t i = 0; i < DW_ARRAY_SIZE(dump); ++i) {
		ptr[i] = dump[i];
	}

	/* dump_POLY_FT4(ft4); */

	setXYWH(ft4, 0, 0, 320, 240);

	pft4->pkt[1] = pft4->pkt[0];

	/* SetDrawOffset(&screen_offset[0], (u_short *)dw_draw[0].ofs); */
	/* screen_offset[1].code[1] = 0xE50B40A0; */

	/* SetDrawOffset(&screen_offset[1], (u_short *)dw_draw[1].ofs); */
	/* screen_offset[0].code[1] = 0xE503C0A0; */
}

void test_map(void)
{
	u_long ot[2][OTSIZE];
	int id = 0;

	screen_quad_init();

	int state = 0;

	while (DrawSync(1));
	VSync(0);

	SetDispMask(1);

	db_font_init();
	SetDumpFnt(bgfont);

	u_long *tim;

	tim = cd_read_alloc("\\ETCDAT\\BI_IN.TIM;1", NULL);
	dw_tim_load(tim);
	xfree(tim);

	tim = cd_read_alloc("\\ETCDAT\\BTLCOMND.TIM;1", NULL);
	dw_tim_load(tim);
	xfree(tim);

	tim = cd_read_alloc("\\ETCDAT\\DEFEFF.TIM;1", NULL);
	dw_tim_load(tim);
	xfree(tim);

	tim = cd_read_alloc("\\ETCDAT\\FEEL_EF.TIM;1", NULL);
	dw_tim_load(tim);
	xfree(tim);

	tim = cd_read_alloc("\\ETCDAT\\FI_INFO.TIM;1", NULL);
	dw_tim_load(tim);
	xfree(tim);

	tim = cd_read_alloc("\\ETCDAT\\ITEM.TIM;1", NULL);
	dw_tim_load(tim);
	xfree(tim);

	tim = cd_read_alloc("\\ETCDAT\\SBOY.TIM;1", NULL);
	dw_tim_load(tim);
	xfree(tim);

	tim = cd_read_alloc("\\ETCDAT\\SPOT256.TIM;1", NULL);
	dw_tim_load(tim);
	xfree(tim);

	tim = cd_read_alloc("\\ETCDAT\\SYSTEM_W.TIM;1", NULL);
	dw_tim_load(tim);
	xfree(tim);

	while (state != 3) {
		bg_disabled = false;
		objects_disabled = false;

		cur_map_id += map_id_change;

		while (map_load(cur_map_id)) {
			if (!map_id_change) {
				map_id_change = 1;
			}
			cur_map_id += map_id_change;
		}

		map_id_change = 0;

		dw_map_get_light_source_matrix(map, &dw_light_source_matrix);
		dw_map_get_light_color_matrix(map, &dw_light_color_matrix);

		char map_name[25];
		sprintf(map_name, "%.24s", dw_get_map_loading_name(map->entry->loading_name_id));

		const char *map_name_start = &map_name[strspn(map_name, " ")];
		char *c;
		for (c = &map_name[24]; c >= map_name && (*c == '\0' || *c == ' '); --c) {
			*c = '\0';
		}

		int16_t player_radius = dw_get_character_params(0)->radius;
		int16_t digimon_radius = dw_get_character_params(entities[1]->character_id)->radius;

		/* dumpMatrix(&dw_light_source_matrix); */
		/* dumpMatrix(&dw_light_color_matrix); */

		SetBackColor(0x80, 0x80, 0x80);

		SetColorMatrix(&dw_light_color_matrix);

		int fcount = 0;

		state = 0;
		screen_fade = 0xa0;

		MATRIX map_m;
		gs_set_ref_view(&map_m, &vp, &vr);

		while (state < 2) {
			int nstate = pad_read();
			if (nstate == 3) {
				state = nstate;
				break;
			}

			if (!state && nstate) {
				state = 1;
			}

			switch (state) {
			case 0:
				if (screen_fade) {
					screen_fade = DW_CLAMP(screen_fade - 2, 0, 0xa0);
				}
				break;
			case 1:
				if (screen_fade < 0xa0) {
					screen_fade = DW_CLAMP(screen_fade + 2, 0, 0xa0);
				} else {
					state = 2;
				}
			}

			static bool digimon_running;
			VECTOR *pv1 = &entities[0]->position;
			VECTOR *pv2 = &entities[1]->position;
			long dx = pv1->vx - pv2->vx;
			dx *= dx;
			long dy = pv1->vy - pv2->vy;
			dy *= dy;
			long dz = pv1->vz - pv2->vz;
			dz *= dz;
			/* long distance = SquareRoot0((dx + dy + dz) >> 4); */
			long distance = (dx + dy + dz) >> 2;
			if (distance > ((player_radius + digimon_radius) * (player_radius + digimon_radius))) {
				if (!digimon_running) {
					dw_entity_play_anim(entities[1], 4);
					digimon_running = true;
				}
			} else {
				if (digimon_running) {
					dw_entity_play_anim(entities[1], 0);
					digimon_running = false;
				}
			}

			dw_world_screen_matrix = map_m;

			if (fcount % 3 == 0) {
				for (size_t i = 0; i < DW_ARRAY_SIZE(entities); ++i) {
					if (entities[i] != NULL) {
						if (i > 0) {
							dw_entity_look_at(entities[i], &entities[0]->position);
						}
						dw_entity_animate(entities[i]);
						dw_entity_update(entities[i]);
					}
				}
			}
			++fcount;

			id = id ? 0 : 1;

			ClearOTagR(ot[id], OTSIZE);

			if (bg_disabled) {
				dw_draw[id].r0 = 60;
				dw_draw[id].g0 = 120;
				dw_draw[id].b0 = 120;
			} else {
				dw_draw[id].r0 = 0;
				dw_draw[id].g0 = 0;
				dw_draw[id].b0 = 0;
			}

			dw_light_matrix = dw_light_source_matrix;
			SetColorMatrix(&dw_light_color_matrix);

			target_position = entities[0]->position;

			SetRotMatrix(&dw_world_screen_matrix);
			SetTransMatrix(&dw_world_screen_matrix);

			SetGeomOffset(320 / 2, 240 / 2);

			SVECTOR v0 = { .vx = vr.vx, .vy = vr.vy, .vz = vr.vz };
			int16_t sxy[2];
			gte_ldv0(&v0);
			gte_rtps();
			gte_stsxy(sxy);

			SVECTOR v1 = { .vx = target_position.vx, .vy = target_position.vy, .vz = target_position.vz };
			int16_t sxy2[2];
			gte_ldv0(&v1);
			gte_rtps();
			gte_stsxy(sxy2);

			int16_t offset_x = sxy[0] - sxy2[0];
			int16_t offset_y = sxy2[1] - sxy[1];

			dw_bg_set_center(bg,
					 -offset_x - (320 / 2),
					 offset_y - (240 / 2));
			int16_t bgpx, bgpy;
			dw_bg_get_position(bg, &bgpx, &bgpy);
			int16_t bgx, bgy;
			dw_bg_get_center(bg, &bgx, &bgy);

			SetGeomOffset(-bgpx, -bgpy);

			dw_bg_update(bg, id);
			if (!bg_disabled) {
				dw_bg_sort(bg, &ot[id][DW_OT_LAYER_MAP_BG_3], id);
			}

			/* for (int i = 0; i < ninst; ++i) { */
			for (int i = ninst - 1; i >= 0 && !objects_disabled; --i) {
				if (!insts[i].enabled) {
					continue;
				}

				int16_t o = insts[i].inst->anim_state[insts[i].frame];

				if (o >= 0) {
					insts[i].sprt[id] = objs[o].sprt;
					setXY0(&insts[i].sprt[id], insts[i].inst->position_x - bgx, insts[i].inst->position_y - bgy);
					int otz;
					/* uint16_t clut = objs[o].obj->clut; */
					if (!(insts[i].inst->flag & 1)) {
						otz = DW_OT_LAYER_MAP_FG_2;
						/* if (clut == 0xffff) { */
						/* 	otz = DW_OT_LAYER_MAP_FG_2; */
						/* } else if (clut < 16) { */
						/* 	otz = DW_OT_LAYER_MAP_FG_2; */
						/* } else { */
						/* 	otz = DW_OT_LAYER_MAP_FG_2; */
						/* } */
					} else {
						otz = DW_OT_LAYER_MAP_BG_2;
						/* if (clut == 0xffff) { */
						/* 	otz = DW_OT_LAYER_MAP_BG_2; */
						/* } else if (clut < 16) { */
						/* 	otz = DW_OT_LAYER_MAP_BG_2; */
						/* } else { */
						/* 	otz = DW_OT_LAYER_MAP_BG_2; */
						/* } */
					}

					insts[i].tpage[id] = objs[o].tpage;
					AddPrim(&ot[id][otz], &insts[i].sprt[id]);
					AddPrim(&ot[id][otz], &insts[i].tpage[id]);
				}

				if (fcount % 3 == 0) {
					++insts[i].counter;
					if (insts[i].counter > insts[i].inst->anim_duration[insts[i].frame]) {
						insts[i].counter = 0;
						insts[i].frame = (insts[i].frame + 1) & 0x7;
						if (insts[i].inst->anim_state[insts[i].frame] == -1) {
							insts[i].frame = 0;
						}
					}
				}
			}

			for (size_t i = 0; i < DW_ARRAY_SIZE(entities); ++i) {
				if (entities[i] == NULL) {
					continue;
				}

				dw_entity_sort(entities[i],
					    &ot[id][DW_OT_LAYER_MODEL],
					    OTLENGTH, id);
			}

			if (screen_fade) {
				setRGB0(&screen_quad.pft4.pkt[id],
					screen_fade,
					screen_fade,
					screen_fade);
				AddPrim(&ot[id][DW_OT_LAYER_MAP_FG_0], &screen_quad.pft4.pkt[id]);
			}

			if (font_enabled) {
				FntPrint("%s\n", map->entry->name);
				FntPrint("%s\n", map_name_start);
				FntPrint("Sta/Sel next\n");
				FntPrint("L1/R1 Pal %d\n", cur_palette);
				FntPrint("O Obj %s\n", objects_disabled ? "off" : "on");
				FntPrint("X BG %s\n", bg_disabled ? "off" : "on");
				FntPrint("[] Hide\n");
				FntPrint("/\\ Back");
			}

			db_swap(&ot[id][OTSIZE - 1], font_enabled);
		}

		while (DrawSync(1));
		VSync(0);

		map_unload();
		check_allocs();
	}
}

static int pad_read(void)
{
	static int start_state;
	static int fcount = 0;

	if (!PAD_DATA_VALID(&pad_port1) ||
	    ((PAD_GET_TYPE(&pad_port1) != STD_PAD) &&
	     (PAD_GET_TYPE(&pad_port1) != ANALOG_JOY))) {
		return 0;
	}

	if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_START)) {
		map_id_change = 1;
		return 1;
	}

	if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_SELECT)) {
		map_id_change = -1;
		return 1;
	}

	font_enabled = !(PAD_KEY_IS_PRESSED(&pad_port1, PAD_RLEFT));

	if (!fcount) {
		if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_L1)) {
			fcount = 20;
			--cur_palette;
			cur_palette = DW_CLAMP(cur_palette, 0,
					       tfs->config->num_palettes - 1);
			load_palette(cur_palette);
			return 0;
		}

		if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_R1)) {
			fcount = 20;
			++cur_palette;
			cur_palette = DW_CLAMP(cur_palette, 0,
					       tfs->config->num_palettes - 1);
			load_palette(cur_palette);
			return 0;
		}
	} else {
		--fcount;
	}

	static bool is_running;
	bool running = (PAD_KEY_IS_PRESSED(&pad_port1, PAD_LUP) ||
			PAD_KEY_IS_PRESSED(&pad_port1, PAD_LDOWN) ||
			PAD_KEY_IS_PRESSED(&pad_port1, PAD_LLEFT) ||
			PAD_KEY_IS_PRESSED(&pad_port1, PAD_LRIGHT));

	if (running != is_running) {
		int anim = running ? 3 : 0;
		dw_entity_play_anim(entities[0], anim);

		is_running = running;
	}

	int direction_x = 0;
	int direction_y = 0;

	if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_LUP)) {
		direction_y = 1;
	} else if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_LDOWN)) {
		direction_y = -1;
	}

	if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_LLEFT)) {
		direction_x = -1;
	} else if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_LRIGHT)) {
		direction_x = 1;
	}

	switch (direction_x) {
	case 0:
		if (direction_y != 0) {
			entities[0]->rotation.vy = direction_y * 1024 + 1024;
		}
		break;
	case -1:
		entities[0]->rotation.vy = direction_y * 512 + 1024;
		break;
	case 1:
		entities[0]->rotation.vy = 3072 - direction_y * 512;
		break;
	}

	if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_RUP)) {
		return 3;
	}

	static bool rdown_pressed;
	static bool rright_pressed;

	bool rright = PAD_KEY_IS_PRESSED(&pad_port1, PAD_RRIGHT);
	bool rdown = PAD_KEY_IS_PRESSED(&pad_port1, PAD_RDOWN);

	if (rright != rright_pressed) {
		if (rright) {
			objects_disabled = !objects_disabled;
		}

		rright_pressed = rright;
	}

	if (rdown != rdown_pressed) {
		if (rdown) {
			bg_disabled = !bg_disabled;
		}

		rdown_pressed = rdown;
	}

	if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_START)) {
		if (!start_state) {
			start_state = 1;
		}
	} else {
		start_state = 0;
	}

	return 0;
}
