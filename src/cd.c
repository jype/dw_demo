/* See LICENSE file for copyright and license details. */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>

#include <libds.h>

#include "alloc.h"
#include "cd.h"
#include "utils.h"

static void cd_read_sync(DslLOC *pos, int sectors, u_long *buf, int mode)
{
	int err;

	do {
		while (!(err = DsRead(pos, sectors, buf, mode)));
		while ((err = DsReadSync(NULL)) > 0);
	} while (err);
}

void
cd_read_sectors(const char *file, uint8_t *buf, int sector_offset,
		int num_sectors)
{
	DslFILE f;
	DslFILE *fp;
	DslLOC pos;
	int i;

	printf("%s: reading %s\n", __func__, file);

	fp = DsSearchFile(&f, (char *)file);
	DW_DEBUG_ASSERT(fp != NULL);

	i = DsPosToInt(&f.pos);
	i += sector_offset;
	DsIntToPos(i, &pos);

	cd_read_sync(&pos, num_sectors, (u_long *)buf, DslModeSpeed);
}

void *cd_read_alloc(const char *file, size_t *size)
{
	DslFILE f;
	DslFILE *fp;
	void *buf;
	size_t sectors;

	printf("%s: loading %s\n", __func__, file);

	fp = DsSearchFile(&f, (char *)file);
	DW_DEBUG_ASSERT(fp != NULL);

	if (size != NULL) {
		*size = f.size;
	}

	sectors = BYTES_TO_SECTORS(f.size);
	buf = xmalloc(sectors * CD_SECTOR_SIZE);

	cd_read_sync(&f.pos, sectors, (u_long *)buf, DslModeSpeed);

	return buf;
}
