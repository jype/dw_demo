/* See LICENSE file for copyright and license details. */

#include <stddef.h>
#include <stdint.h>

#include <sys/types.h>

#include <libgte.h>

#include "gs.h"
#include "utils.h"

static MATRIX matrix_identity = {
	.m = {
		{ 4096, 0, 0 },
		{ 0, 4096, 0 },
		{ 0, 0, 4096 },
	},
	.t = { 0, 0, 0 },
};

static int32_t vector_abs_max(const VECTOR *v)
{
	int32_t a;
	int32_t b;

	a = DW_ABS(v->vx);
	b = DW_ABS(v->vy);
	a = DW_MAX(a, b);
	b = DW_ABS(v->vz);

	return DW_MAX(a, b);
}

static uint8_t msb_int32(int32_t v)
{
	uint8_t i;

	for (i = 0; v; ++i, v >>= 1);

	return i;
}

static void vpvr_scale(int32_t *v, const VECTOR *vp, const VECTOR *vr)
{
	int32_t a = vector_abs_max(vp);
	int32_t b = vector_abs_max(vr);
	int32_t scale;

	scale = msb_int32(DW_MAX(a, b)) - 15;
	DW_DEBUG_ASSERT(scale < 16);
	(void)v;
	/* if (scale < 16) { */
	/* 	v[0] = vp->vx; */
	/* 	v[1] = vp->vy; */
	/* 	v[2] = vp->vz; */
	/* 	v[3] = vr->vx; */
	/* 	v[4] = vr->vy; */
	/* 	v[5] = vr->vz; */
	/* } else { */
	/* 	v[0] = vp->vx >> scale; */
	/* 	v[1] = vp->vy >> scale; */
	/* 	v[2] = vp->vz >> scale; */
	/* 	v[3] = vr->vx >> scale; */
	/* 	v[4] = vr->vy >> scale; */
	/* 	v[5] = vr->vz >> scale; */
	/* } */
}

#define GS_AXIS_X	0
#define GS_AXIS_Y	1
#define GS_AXIS_Z	2

static void matrix_make(MATRIX *m, int16_t sina, int16_t cosa, int axis)
{
	*m = matrix_identity;

	switch (axis) {
	case GS_AXIS_X:
		m->m[1][1] = cosa;
		m->m[1][2] = -sina;
		m->m[2][1] = sina;
		m->m[2][2] = cosa;
		break;
	case GS_AXIS_Y:
		m->m[0][0] = cosa;
		m->m[0][2] = sina;
		m->m[2][0] = -sina;
		m->m[2][2] = cosa;
		break;
	case GS_AXIS_Z:
		m->m[0][0] = cosa;
		m->m[0][1] = -sina;
		m->m[1][0] = sina;
		m->m[1][1] = cosa;
		break;
	default:
		DW_DEBUG_ASSERT(0);
		break;
	}
}

void gs_set_ref_view(MATRIX *m, const VECTOR *vp, const VECTOR *vr)
{
	MATRIX tmp_m;
	VECTOR forward;
	VECTOR v1;
	VECTOR v2;
	int32_t vpvr_length;
	int32_t vpvr_xz_length;
	int32_t vpvr_z_one;
	int32_t vpvr_y_one;

	vpvr_scale(NULL, vp, vr);

	*m = matrix_identity;

	forward.vx = vr->vx - vp->vx;
	forward.vy = vr->vy - vp->vy;
	forward.vz = vr->vz - vp->vz;

	vpvr_length = SquareRoot0((forward.vx * forward.vx) +
				  (forward.vy * forward.vy) +
				  (forward.vz * forward.vz));
	DW_DEBUG_ASSERT(vpvr_length);

	vpvr_y_one = forward.vy * ONE;

	vpvr_xz_length = SquareRoot0((forward.vx * forward.vx) +
				     (forward.vz * forward.vz));
	DW_DEBUG_ASSERT(vpvr_xz_length);

	matrix_make(&tmp_m, vpvr_y_one / vpvr_length,
		    (vpvr_xz_length * ONE) / vpvr_length,
		    GS_AXIS_X);
	MulMatrix(m, &tmp_m);

	vpvr_y_one = forward.vx * ONE;
	vpvr_z_one = forward.vz * ONE;

	matrix_make(&tmp_m, -vpvr_y_one / vpvr_xz_length,
		    (vpvr_z_one / vpvr_xz_length),
		    GS_AXIS_Y);
	MulMatrix(m, &tmp_m);

	v1.vx = -vp->vx;
	v1.vy = -vp->vy;
	v1.vz = -vp->vz;

	ApplyMatrixLV(m, &v1, &v2);
	TransMatrix(m, &v2);
}
