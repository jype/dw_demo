/* See LICENSE file for copyright and license details. */

#include <stdint.h>

#include <sys/types.h>

#include <libgte.h>
#include <libgpu.h>

#include "alloc.h"
#include "bg.h"
#include "map.h"
#include "tfs.h"
#include "utils.h"

#define DW_BG_TILE_WIDTH	4
#define DW_BG_TILE_HEIGHT	3
#define DW_BG_TILE_DIM		128
#define DW_BG_WIDTH		((DW_BG_TILE_WIDTH) * DW_BG_TILE_DIM)
#define DW_BG_HEIGHT		((DW_BG_TILE_HEIGHT) * DW_BG_TILE_DIM)

typedef struct {
	SPRT sprt[2];
	DR_TPAGE tpage[2];
	RECT rect;
	int16_t position_x;
	int16_t position_y;
	uint32_t image_index;
} dw_bg_tile;

struct dw_bg {
	dw_bg_tile tiles[DW_BG_TILE_WIDTH][DW_BG_TILE_HEIGHT];
	const dw_map *map;
	const dw_tfs *tfs;
	int16_t position_x;
	int16_t position_y;
	uint8_t width;
	uint8_t height;
	uint16_t display_width;
	uint16_t display_height;
};

dw_bg *dw_bg_init(const dw_map *map, const dw_tfs *tfs,
		  uint16_t display_width, uint16_t display_height)
{
	dw_bg *bg;
	dw_bg_tile *tile;
	int tpage;
	uint8_t i;
	uint8_t j;

	bg = xcalloc(1, sizeof(*bg));
	bg->map = map;
	bg->tfs = tfs;
	bg->width = DW_MIN(map->config->width, DW_BG_TILE_WIDTH);
	bg->height = DW_MIN(map->config->height, DW_BG_TILE_HEIGHT);
	bg->display_width = display_width;
	bg->display_height = display_height;

	for (i = 0; i < bg->width; ++i) {
		for (j = 0; j < bg->height; ++j) {
			tile = &bg->tiles[i][j];

			tile->rect.x = 768 + i * (DW_BG_TILE_DIM / 2);
			tile->rect.y = j * DW_BG_TILE_DIM;
			tile->rect.w = (DW_BG_TILE_DIM / 2);
			tile->rect.h = DW_BG_TILE_DIM;

			tile->image_index = UINT32_MAX;

			setSprt(&tile->sprt[0]);
			setRGB0(&tile->sprt[0], 0x80, 0x80, 0x80);
			setUV0(&tile->sprt[0], 0, j * DW_BG_TILE_DIM);
			setClut(&tile->sprt[0], 0, 481);
			setWH(&tile->sprt[0], DW_BG_TILE_DIM, DW_BG_TILE_DIM);

			tpage = getTPage(1, 0,
					 768 + i * (DW_BG_TILE_DIM / 2),
					 j * DW_BG_TILE_DIM);
			SetDrawTPage(&tile->tpage[0], 0, 1, tpage);

			dw_bg_update(bg, 0);

			tile->sprt[1] = tile->sprt[0];
			tile->tpage[1] = tile->tpage[0];
		}
	}

	return bg;
}

void dw_bg_deinit(dw_bg *bg)
{
	xfree(bg);
}

void dw_bg_get_position(dw_bg *bg, int16_t *x, int16_t *y)
{
	const int16_t w = bg->map->config->width * DW_BG_TILE_DIM;
	const int16_t h = bg->map->config->height * DW_BG_TILE_DIM;

	*x = bg->position_x - (w / 2);
	*y = bg->position_y - (h / 2);
}

void dw_bg_get_center(dw_bg *bg, int16_t *x, int16_t *y)
{
	*x = bg->position_x;
	*y = bg->position_y;
}

void dw_bg_set_center(dw_bg *bg, int16_t x, int16_t y)
{
	const int16_t w = bg->map->config->width * DW_BG_TILE_DIM;
	const int16_t h = bg->map->config->height * DW_BG_TILE_DIM;

	x += w / 2;
	y += h / 2;

	bg->position_x = DW_CLAMP(x, 0, w - bg->display_width);
	bg->position_y = DW_CLAMP(y, 0, h - bg->display_height);
}

static void dw_bg_tile_update(dw_bg *bg, dw_bg_tile *tile, int id)
{
	dw_tfs_image *images = bg->tfs->images;
	int16_t image_index_x;
	int16_t image_index_y;
	uint32_t image_index;

	setXY0(&tile->sprt[id], tile->position_x, tile->position_y);

	image_index_x = ((bg->position_x + tile->position_x) / DW_BG_TILE_DIM);
	image_index_y = ((bg->position_y + tile->position_y) / DW_BG_TILE_DIM);

	if ((image_index_x >= 0) &&
	    ((uint32_t)image_index_x < bg->map->config->width) &&
	    (image_index_y >= 0) &&
	    ((uint32_t)image_index_y < bg->map->config->height)) {
		image_index = bg->map->config->tiles[(image_index_y *
						      bg->map->config->width +
						      image_index_x)];
		if ((image_index != tile->image_index) &&
		    (image_index != UINT32_MAX)) {
			LoadImage(&tile->rect,
				  (u_long *)images[image_index].data);
		}

		tile->image_index = image_index;
	}
}

void dw_bg_update(dw_bg *bg, int id)
{
	dw_bg_tile *tile;
	int16_t left;
	int16_t right;
	int16_t top;
	int16_t bottom;
	uint8_t i;
	uint8_t j;

	for (i = 0; i < bg->width; ++i) {
		for (j = 0; j < bg->height; ++j) {
			tile = &bg->tiles[i][j];

			left = (-bg->position_x + (i * DW_BG_TILE_DIM));
			right = left + DW_BG_TILE_DIM - 1;
			top = (-bg->position_y + (j * DW_BG_TILE_DIM));
			bottom = top + DW_BG_TILE_DIM - 1;

			tile->position_x = (left +
					    ((DW_BG_WIDTH - right - 1) /
					     DW_BG_WIDTH) * DW_BG_WIDTH);
			tile->position_y = (top +
					    ((DW_BG_HEIGHT - bottom - 1) /
					     DW_BG_HEIGHT) * DW_BG_HEIGHT);

			dw_bg_tile_update(bg, tile, id);
		}
	}
}

void dw_bg_sort(dw_bg *bg, u_long *ot, int id)
{
	uint8_t i;
	uint8_t j;

	for (i = 0; i < bg->width; ++i) {
		for (j = 0; j < bg->height; ++j) {
			if (bg->tiles[i][j].image_index == UINT32_MAX) {
				continue;
			}

			AddPrim(ot, (void *)&bg->tiles[i][j].sprt[id]);
			AddPrim(ot, (void *)&bg->tiles[i][j].tpage[id]);
		}
	}
}
