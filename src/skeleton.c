/* See LICENSE file for copyright and license details. */

#include <stdint.h>

#include "alloc.h"
#include "bin.h"
#include "skeleton.h"
#include "params.h"
#include "utils.h"

static uint32_t dw_skeleton_get_offset(uint32_t character_id)
{
	return ((uint32_t *)&skeleton_node_offset_start)[character_id];
}

static dw_bone *dw_skeleton_get_node(uint8_t character_id)
{
	dw_bone *nodes;
	uint32_t skel_offset;

	DW_DEBUG_ASSERT(character_id < 180);

	skel_offset = dw_skeleton_get_offset(character_id);
	nodes = (dw_bone *)&skeleton_node_start;

	return &nodes[skel_offset / sizeof(dw_bone)];
}

dw_skeleton *dw_skeleton_init(uint8_t character_id)
{
	dw_skeleton *skeleton;
	uint32_t num_nodes = dw_get_character_params(character_id)->num_bones;

	skeleton = xcalloc(1, (sizeof(*skeleton) +
			       (num_nodes * sizeof(dw_node))));
	skeleton->root = dw_skeleton_get_node(character_id);
	skeleton->num_nodes = num_nodes;

	return skeleton;
}

void dw_skeleton_deinit(dw_skeleton *skeleton)
{
	xfree(skeleton);
}
