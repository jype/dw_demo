/* See LICENSE file for copyright and license details. */

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

#include <sys/types.h>

#include <libgte.h>

#include "alloc.h"
#include "cd.h"
#include "entity.h"
#include "graphics.h"
#include "params.h"
#include "pmd.h"
#include "quad.h"
#include "skeleton.h"
#include "tim.h"
#include "utils.h"

#define ALLTIM_ENTRY_SIZE	0x4800
#define ALLTIM_SECTOR_SIZE	((ALLTIM_ENTRY_SIZE) / (CD_SECTOR_SIZE))

static void cd_read_alltim(uint8_t *buf, uint8_t character_id)
{
	cd_read_sectors("\\CHDAT\\ALLTIM.TIM;1", buf,
			character_id * ALLTIM_SECTOR_SIZE,
			ALLTIM_SECTOR_SIZE);
}

__attribute__((aligned(8)))
static uint8_t buf[ALLTIM_ENTRY_SIZE];

dw_entity *dw_entity_init(uint8_t character_id, uint8_t slot)
{
	dw_entity *ent;
	dw_skeleton *skeleton;
	pmd_model *model;
	dw_mtn *mtn;
	dw_node *node;
	const dw_bone *bone;
	uint32_t i;

	DW_DEBUG_ASSERT((character_id != 62) && (character_id != 114));

	skeleton = dw_skeleton_init(character_id);
	DW_DEBUG_ASSERT(skeleton->num_nodes > 0);

	char name[256];
	sprintf(name, "\\CHDAT\\PMD%d\\%s.PMD;1", character_id / 30,
		dw_get_character_model_name(character_id));

	model = cd_read_alloc(name, NULL);
	pmd_init(model);

	sprintf(name, "\\CHDAT\\MTN%d\\%s.MTN;1", character_id / 30,
		dw_get_character_model_name(character_id));

	TIM_IMAGE image;

	cd_read_alltim(buf, character_id);

	DW_DEBUG_ASSERT(!OpenTIM((u_long *)buf));
	DW_DEBUG_ASSERT(ReadTIM(&image) != NULL);

	DW_DEBUG_ASSERT(image.paddr != NULL);
	DW_DEBUG_ASSERT(image.prect != NULL);
	DW_DEBUG_ASSERT(image.prect->w <= 64);
	DW_DEBUG_ASSERT(image.prect->h <= 128);

	RECT prect = {
		.x = (slot % 3) * 64 + 320,
		.y = (slot / 3) * 128 + 256,
		.w = image.prect->w,
		.h = image.prect->h,
	};

	DW_DEBUG_ASSERT(image.caddr != NULL);
	DW_DEBUG_ASSERT(image.crect != NULL);
	DW_DEBUG_ASSERT(image.crect->w == 16);
	DW_DEBUG_ASSERT(image.crect->h <= 16);

	RECT crect = {
		.x = slot * 16,
		.y = 488,
		.w = image.crect->w,
		.h = image.crect->h,
	};

	dw_tim_load_rect((u_long *)buf, &prect, &crect);

	uint8_t *mtn_data = cd_read_alloc(name, NULL);
	mtn = dw_mtn_init(mtn_data, skeleton->num_nodes, &prect);

	for (i = 0; i < skeleton->num_nodes; ++i) {
		bone = &skeleton->root[i];

		DW_DEBUG_ASSERT(!i || (bone->parent != 255));

		if (bone->object != 255) {
			node = &skeleton->nodes[i];
			node->object = pmd_get_object(model, bone->object);
			DW_DEBUG_ASSERT(node->object != NULL);
			pmd_object_set_texture(node->object,
					       prect.x, prect.y,
					       crect.x - image.crect->x,
					       crect.y - image.crect->y);
		}
	}

	ent = xcalloc(1, sizeof(*ent));
	ent->skeleton = skeleton;
	ent->model = model;
	ent->mtn = mtn;
	ent->character_id = character_id;

	dw_params *params = dw_get_character_params(character_id);
	int16_t radius = params->radius;

	dw_quad_ft4_init(&ent->shadow,
			 DW_PLANE_XZ,
			 -radius, -radius,
			 radius * 2, radius * 2);

	pmd_poly_ft4 *pft4 = &ent->shadow.pft4;

	POLY_FT4 *ft4 = &pft4->pkt[0];
	setRGB0(ft4, 0x30, 0x30, 0x30);
	setUVWH(ft4, 64, 128, 63, 63);
	setTPage(ft4, 1, 2, 864, 384);
	setClut(ft4, 0, 487);
	setSemiTrans(ft4, 1);

	pft4->pkt[1] = pft4->pkt[0];

	return ent;
}

void dw_entity_deinit(dw_entity *ent)
{
	dw_skeleton_deinit(ent->skeleton);
	xfree(ent->model);
	dw_mtn_deinit(ent->mtn);
	xfree(ent);
}

void dw_entity_look_at(dw_entity *ent, VECTOR *v)
{
	ent->rotation.vx = 0;
	ent->rotation.vy = ratan2((ent->position.vx - v->vx) >> 4,
				  (ent->position.vz - v->vz) >> 4);
	ent->rotation.vz = 0;
}

void dw_entity_play_anim(dw_entity *ent, uint8_t mtn_id)
{
	dw_mtn_play(ent->mtn, mtn_id);
}

void dw_entity_animate(dw_entity *ent)
{
	MATRIX m;
	VECTOR root_motion;
	VECTOR v;

	dw_mtn_run(ent->mtn);
	dw_mtn_apply(ent->mtn, ent->skeleton);

	RotMatrix(&ent->rotation, &m);

	dw_mtn_get_root_motion(ent->mtn, &root_motion);

	ApplyMatrixLV(&m, &root_motion, &v);
	ent->position.vx += v.vx;
	ent->position.vy += v.vy;
	ent->position.vz += v.vz;
}

void dw_entity_update(dw_entity *ent)
{
	dw_node *parent;
	dw_node *node;
	const dw_bone *bone;
	MATRIX ws;
	const MATRIX *m;
	VECTOR v;
	uint32_t i;

	ws = dw_world_screen_matrix;

	ApplyMatrixLV(&ws, &ent->position, &v);

	MATRIX bla;
	RotMatrix(&ent->rotation, &bla);
	MulMatrix(&ws, &bla);

	ws.t[0] += v.vx;
	ws.t[1] += v.vy;
	ws.t[2] += v.vz;

	for (i = 0; i < ent->skeleton->num_nodes; ++i) {
		node = &ent->skeleton->nodes[i];
		bone = &ent->skeleton->root[i];

		RotMatrix(&node->rotation, &node->lw);
		ScaleMatrix(&node->lw, &node->scale);
		/* TransMatrix(&node->lw, &node->position); */

		parent = NULL;
		if (bone->parent != 255) {
			parent = &ent->skeleton->nodes[bone->parent];
			m = &parent->ls;
		} else {
			m = &ws;
		}

		SetRotMatrix((MATRIX *)m);

		MulRotMatrix0(&node->lw, &node->ls);
		ApplyRotMatrixLV(&node->position, &v);

		v.vx += m->t[0];
		v.vy += m->t[1];
		v.vz += m->t[2];
		TransMatrix(&node->ls, &v);

		if (parent != NULL) {
			MulMatrix2(&parent->lw, &node->lw);
		}
	}
}

void dw_entity_sort(dw_entity *ent, u_long *ot, int otlen, int id)
{
	dw_node *node;
	MATRIX light;
	uint32_t i;

	for (i = 0; i < ent->skeleton->num_nodes; ++i) {
		node = &ent->skeleton->nodes[i];
		if (node->object == NULL) {
			continue;
		}

		light = dw_light_matrix;
		MulMatrix(&light, &node->lw);
		SetLightMatrix(&light);

		SetRotMatrix(&node->ls);
		SetTransMatrix(&node->ls);

		pmd_object_sort(node->object, ot, otlen, id);
	}

	MATRIX ws = dw_world_screen_matrix;
	VECTOR v;

	SetRotMatrix(&ws);

	ApplyRotMatrixLV(&ent->position, &v);

	ws.t[0] += v.vx;
	ws.t[1] += v.vy;
	ws.t[2] += v.vz;

	SetTransMatrix(&ws);

	pmd_object_sort(&ent->shadow.object, &ot[(1 << otlen) - 1], 0, id);
}
