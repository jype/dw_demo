/* See LICENSE file for copyright and license details. */

#include <stdint.h>

#include <sys/types.h>

#include <libgte.h>
#include <libgpu.h>

#include "pmd.h"
#include "quad.h"
#include "utils.h"

void dw_quad_ft4_init(dw_quad_ft4 *qft4,
		      dw_plane plane,
		      int16_t x, int16_t y,
		      int16_t w, int16_t h)
{
	pmd_poly_ft4 *pft4;

	qft4->object.nptr = 1;
	qft4->ptr = &qft4->type;
	qft4->type = PMD_MK_TYPE(1, PMD_TYPE_QUAD);

	pft4 = &qft4->pft4;

	setPolyFT4(&pft4->pkt[0]);
	setPolyFT4(&pft4->pkt[1]);

	switch (plane) {
	case DW_PLANE_XY:
		pft4->v1 = (SVECTOR){ x, y + h, 0, 0 };
		pft4->v2 = (SVECTOR){ x + w, y + h, 0, 0 };
		pft4->v3 = (SVECTOR){ x, y, 0, 0 };
		pft4->v4 = (SVECTOR){ x + w, y, 0, 0 };
		break;
	case DW_PLANE_XZ:
		pft4->v1 = (SVECTOR){ x, 0, y + h, 0 };
		pft4->v2 = (SVECTOR){ x + w, 0, y + h, 0 };
		pft4->v3 = (SVECTOR){ x, 0, y, 0 };
		pft4->v4 = (SVECTOR){ x + w, 0, y, 0 };
		break;
	case DW_PLANE_YZ:
		pft4->v1 = (SVECTOR){ 0, x, y + h, 0 };
		pft4->v2 = (SVECTOR){ 0, x + w, y + h, 0 };
		pft4->v3 = (SVECTOR){ 0, x, y, 0 };
		pft4->v4 = (SVECTOR){ 0, x + w, y, 0 };
		break;
	default:
		DW_DEBUG_ASSERT(0);
		break;
	}
}
