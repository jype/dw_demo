/* See LICENSE file for copyright and license details. */

.macro		DEFINE_BIN name, section, alignment, filename
.section	\section
.balign		\alignment
.global		\name\()_start
.type		\name\()_start, @object
.size		\name\()_start, \name\()_end - \name\()_start
.global		\name\()_end
.type		\name\()_end, @object
.size		\name\()_end, 0
\name\()_start:
.incbin "\filename"
\name\()_end:
.endm

DEFINE_BIN character_model_name, .rodata, 4, "build/data/character_model_name.bin"
DEFINE_BIN character_params, .rodata, 4, "build/data/character_params.bin"
DEFINE_BIN font, .rodata, 4, "build/data/font.bin"
DEFINE_BIN font_clut, .rodata, 4, "build/data/font_clut.bin"
DEFINE_BIN map_doors, .rodata, 4, "build/data/map_doors.bin"
DEFINE_BIN map_entry, .rodata, 4, "build/data/map_entry.bin"
DEFINE_BIN map_loading_name, .rodata, 4, "build/data/map_loading_name.bin"
DEFINE_BIN map_loading_name_offset, .rodata, 4, "build/data/map_loading_name_offset.bin"
DEFINE_BIN map_toilet, .rodata, 4, "build/data/map_toilet.bin"
DEFINE_BIN skeleton_node, .rodata, 4, "build/data/skeleton_node.bin"
DEFINE_BIN skeleton_node_offset, .rodata, 4, "build/data/skeleton_node_offset.bin"
