/* See LICENSE file for copyright and license details. */

#include <stdint.h>
#include <stdio.h>

#include <sys/types.h>

#include <libgte.h>
#include <libgpu.h>

#include "alloc.h"
#include "bin.h"
#include "cd.h"
#include "map.h"
#include "utils.h"

static void load_object_tim(u_long *addr)
{
	TIM_IMAGE image;
	RECT rect;

	OpenTIM(addr);			/* open TIM */

	while (ReadTIM(&image)) {
		if (image.caddr) {	/* load CLUT (if needed) */
			/* setSTP(image.caddr, image.crect->w); */
			rect = *image.crect;
			if (rect.y == 486) {
				rect.w *= rect.h;
				rect.h = 1;
			}
			/* dumpRECT(&rect); */
			LoadImage(&rect, image.caddr);
		}
		if (image.paddr) {	/* load texture pattern */
			LoadImage(image.prect, image.paddr);
		}
	}
}

dw_map_entry *dw_get_map_entry(uint8_t map_id)
{
	size_t offset = map_id * sizeof(dw_map_entry);
	return (dw_map_entry *)&map_entry_start[offset];
}

dw_map_toilet *dw_get_map_toilet(uint8_t toilet_id)
{
	size_t offset = toilet_id * sizeof(dw_map_toilet);
	return (dw_map_toilet *)&map_toilet_start[offset];
}

dw_map_doors *dw_get_map_doors(uint8_t doors_id)
{
	size_t offset = doors_id * sizeof(dw_map_doors);
	return (dw_map_doors *)&map_doors_start[offset];
}

const char *dw_get_map_loading_name(uint8_t loading_name_id)
{
	uint32_t offset = ((uint32_t *)&map_loading_name_offset_start)[loading_name_id];
	return (const char *)&map_loading_name_start[offset];
}

dw_map *dw_map_init(uint8_t map_id)
{
	char name[256] = {0};
	dw_map *map;
	dw_map_entry *entry;
	uint8_t *map_data;
	uint32_t *map_ptr;
	uint32_t objects_offset;
	uint32_t elements_offset;
	uint32_t tile_map_offset;
	uint16_t total_objects;
	uint16_t i;

	map = xcalloc(1, sizeof(dw_map));

	entry = dw_get_map_entry(map_id);
	map->entry = entry;

	if (entry->toilet_id) {
		map->toilet = dw_get_map_toilet(entry->toilet_id - 1);
	}

	if (entry->doors_id) {
		map->doors = dw_get_map_doors(entry->doors_id - 1);
	}

	total_objects = entry->num_images + entry->num_objects;

	sprintf(name, "\\MAP\\MAP%d\\%s.MAP;1",
		(map_id / 15) + 1, entry->name);
	printf("Loading map %s\n", name);

	map_data = cd_read_alloc(name, NULL);
	map->map_data = map_data;

	map_ptr = (uint32_t *)map_data;
	map->config = (dw_map_config *)&map_data[map_ptr[0]];
	++map_ptr;

	for (i = 0; i < total_objects; ++i, ++map_ptr) {
		load_object_tim((u_long *)&map_data[map_ptr[0]]);
	}

	objects_offset = 0;
	if (total_objects) {
		objects_offset = map_ptr[0];
		map->objects = (dw_map_objects *)&map_data[objects_offset];
		map->instances = (dw_map_instances *)
			&map->objects->objects[map->objects->num_objects];
		++map_ptr;
	}

	elements_offset = map_ptr[0];
	map->elements = (dw_map_elements *)&map_data[elements_offset];
	++map_ptr;

	tile_map_offset = map_ptr[0];
	map->tile_map = &map_data[tile_map_offset];
	++map_ptr;

	return map;
}

void dw_map_deinit(dw_map *map)
{
	xfree(map->map_data);
	xfree(map);
}

static inline int32_t vector_magnitude(const VECTOR *v)
{
	return SquareRoot0((v->vx * v->vx) +
			   (v->vy * v->vy) +
			   (v->vz * v->vz));
}

void dw_map_get_light_source_matrix(const dw_map *map, MATRIX *m)
{
	dw_map_light *light;
	int32_t magnitude;
	size_t i;

	for (i = 0; i < 3; ++i) {
		light = &map->config->lights[i];
		magnitude = vector_magnitude((const VECTOR *)&light->vx);
		if(magnitude) {
			m->m[i][0] = (light->vx * -ONE) / magnitude;
			m->m[i][1] = (light->vy * -ONE) / magnitude;
			m->m[i][2] = (light->vz * -ONE) / magnitude;
		} else {
			m->m[i][0] = 0;
			m->m[i][1] = 0;
			m->m[i][2] = 0;
		}
	}
}

void dw_map_get_light_color_matrix(const dw_map *map, MATRIX *m)
{
	dw_map_light *light;
	size_t i;

	for (i = 0; i < 3; ++i) {
		light = &map->config->lights[i];
		m->m[0][i] = (light->r * ONE) / UINT8_MAX;
		m->m[1][i] = (light->g * ONE) / UINT8_MAX;
		m->m[2][i] = (light->b * ONE) / UINT8_MAX;
	}
}
