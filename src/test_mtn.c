/* See LICENSE file for copyright and license details. */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>

#include <libapi.h>
#include <libds.h>
#include <libetc.h>
#include <libgte.h>
#include <libgpu.h>

#include "alloc.h"
#include "bg.h"
#include "bin.h"
#include "cd.h"
#include "entity.h"
#include "graphics.h"
#include "inline_n.h"
#include "map.h"
#include "pmd.h"
#include "skeleton.h"
#include "tfs.h"
#include "tim.h"
#include "quad.h"
#include "utils.h"

#define SCR_Z			1024
#define OTLENGTH		14
#define OTSIZE			(1 << (OTLENGTH))

#include "params.h"
#include "skeleton.h"

static u_long ot[2][OTSIZE];

static SVECTOR ang = { 0 };
static VECTOR vec = { 0, 0, SCR_Z * 3, 0 };

static int pad_read(void);

static int16_t cur_id = 0;
static int cur_id_change;
static uint8_t cur_anim_id;

static SVECTOR lgtang = { 0 };

static dw_entity *ent;

static int entity_load(uint8_t character_id)
{
	if ((character_id == 62) || (character_id == 114) || (character_id >= 180)) {
		printf("Skipping entity %d\n", character_id);
		return 1;
	}

	ent = dw_entity_init(character_id, 0);

	return 0;
}

static void entity_unload(void)
{
	if (ent != NULL) {
		dw_entity_deinit(ent);
		ent = NULL;
	}
}

/* Local color matrix */
static MATRIX   cmat = {
	.m = {
		{ ONE / 2,  0,  0 },
		{ ONE / 2,  0,  0 },
		{ ONE / 2,  0,  0 },
	},
};

/* Local light matrix */
static MATRIX lgtmat = {
	.m = {
		{ ONE,  -ONE,  -ONE },
		{ 0,    0,    0  },
		{ 0,    0,    0  },
	},
};

static bool font_enabled = true;
static bool light_enabled = false;

static void set_lights(void)
{
	if (light_enabled) {
		dw_light_source_matrix = lgtmat;
		dw_light_color_matrix = cmat;
		SetBackColor(0x80, 0x80, 0x80);
	} else {
		memset(&dw_light_source_matrix, 0, sizeof(dw_light_source_matrix));
		memset(&dw_light_color_matrix, 0, sizeof(dw_light_color_matrix));
		SetBackColor(0xff, 0xff, 0xff);
	}
}

void test_mtn(void)
{
	int id = 0;

	memset(&dw_world_screen_matrix, 0, sizeof(dw_world_screen_matrix));
	memset(&dw_light_source_matrix, 0, sizeof(dw_light_source_matrix));
	memset(&dw_light_color_matrix, 0, sizeof(dw_light_color_matrix));
	memset(&dw_light_matrix, 0, sizeof(dw_light_matrix));

	int state = 0;

	while (DrawSync(1));
	VSync(0);

	SetDispMask(1);

	db_font_init();
	SetDumpFnt(bgfont);

	u_long *tim;

	tim = cd_read_alloc("\\ETCDAT\\SPOT256.TIM;1", NULL);
	dw_tim_load(tim);
	xfree(tim);

	while (state != 2) {
		cur_id += cur_id_change;
		if (cur_id < 0) {
			cur_id = 179;
		} else if (cur_id >= 180) {
			cur_id = 0;
		}

		cur_anim_id = 0;

		while (entity_load(cur_id)) {
			if (!cur_id_change) {
				cur_id_change = 1;
			}
			cur_id += cur_id_change;
		}

		dw_params *params = dw_get_character_params(cur_id);
		const char *name = dw_get_character_model_name(cur_id);

		cur_id_change = 0;

		int fcount = 0;

		state = 0;

		set_lights();

		while (!(state = pad_read())) {
			if (fcount % 3 == 0) {
				dw_entity_animate(ent);
				ent->position = (VECTOR){ 0, params->height / 2, 0, 0 };
				dw_entity_update(ent);
			}
			++fcount;

			id = id ? 0 : 1;

			ClearOTagR(ot[id], OTSIZE);

			dw_draw[id].r0 = 60;
			dw_draw[id].g0 = 120;
			dw_draw[id].b0 = 120;

			MATRIX rotlgt;
			RotMatrix(&lgtang, &rotlgt);
			MulMatrix0(&dw_light_source_matrix, &rotlgt,
				   &dw_light_matrix);
			dw_light_matrix = dw_light_source_matrix;
			SetColorMatrix(&dw_light_color_matrix);

			RotMatrix(&ang, &dw_world_screen_matrix);
			TransMatrix(&dw_world_screen_matrix, &vec);

			SetRotMatrix(&dw_world_screen_matrix);
			SetTransMatrix(&dw_world_screen_matrix);

			SetGeomOffset(320 / 2,240 / 2);

			dw_entity_sort(ent, ot[id], OTLENGTH, id);

			if (font_enabled) {
				FntPrint("%s\n", name);
				FntPrint("%s\n", params->name);
				FntPrint("Frame %d\n", dw_mtn_get_frame(ent->mtn));
				FntPrint("Sta/Sel next\n");
				FntPrint("L1/R1 Anim %d\n", cur_anim_id);
				FntPrint("L2/R2 Zoom\n");
				FntPrint("O Lights %s\n", light_enabled ? "on" : "off");
				FntPrint("[] Hide\n");
				FntPrint("/\\ Back");
			}

			db_swap(&ot[id][OTSIZE - 1], font_enabled);
		}

		while (DrawSync(1));
		VSync(0);

		entity_unload();
		check_allocs();
	}
}

static int pad_read(void)
{
	static int fcount = 0;

	if (!PAD_DATA_VALID(&pad_port1) ||
	    ((PAD_GET_TYPE(&pad_port1) != STD_PAD) &&
	     (PAD_GET_TYPE(&pad_port1) != ANALOG_JOY))) {
		return 0;
	}

	if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_START)) {
		cur_id_change = 1;
		return 1;
	}

	if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_SELECT)) {
		cur_id_change = -1;
		return 1;
	}

	font_enabled = !(PAD_KEY_IS_PRESSED(&pad_port1, PAD_RLEFT));

	if (!fcount) {
		if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_L1)) {
			fcount = 20;
			cur_anim_id = DW_CLAMP(cur_anim_id - 1, 0, dw_mtn_get_num_anim(ent->mtn) - 1);
			dw_entity_play_anim(ent, cur_anim_id);
			return 0;
		}

		if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_R1)) {
			fcount = 20;
			cur_anim_id = DW_CLAMP(cur_anim_id + 1, 0, dw_mtn_get_num_anim(ent->mtn) - 1);
			dw_entity_play_anim(ent, cur_anim_id);
			return 0;
		}

		if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_RRIGHT)) {
			fcount = 20;
			light_enabled = !light_enabled;
			set_lights();
		}
	} else {
		--fcount;
	}

	/* if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_RUP))	lgtang.vx += 32; */
	/* if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_RDOWN))	lgtang.vx -= 32; */
	/* if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_RLEFT))	lgtang.vy += 32; */
	/* if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_RRIGHT))	lgtang.vy -= 32; */

	if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_LUP))	ang.vx -= 16;
	if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_LDOWN))	ang.vx += 16;
	if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_LLEFT))	ang.vy += 16;
	if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_LRIGHT))	ang.vy -= 16;
	if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_L2))	vec.vz += 16;
	if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_R2))	vec.vz -= 16;

	if (PAD_KEY_IS_PRESSED(&pad_port1, PAD_RUP)) {
		return 2;
	}

	return 0;
}
