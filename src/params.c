/* See LICENSE file for copyright and license details. */

#include <stdint.h>

#include "bin.h"
#include "params.h"
#include "utils.h"

dw_params *dw_get_character_params(uint8_t id)
{
	DW_DEBUG_ASSERT(id < DW_NUM_CHARACTER_PARAMS);

	return &((dw_params *)&character_params_start)[id];
}

const char *dw_get_character_model_name(uint8_t id)
{
	DW_DEBUG_ASSERT(id < DW_NUM_CHARACTER_PARAMS);

	return &((const char *)&character_model_name_start)[id * 8];
}
