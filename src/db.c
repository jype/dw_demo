/* See LICENSE file for copyright and license details. */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>

#include <libetc.h>
#include <libgte.h>
#include <libgpu.h>
#include <libpad.h>

#include "pad.h"
#include "utils.h"

static int id = 0;
DRAWENV dw_draw[2];
DISPENV dw_disp[2];

controller_packet pad_port1;
controller_packet pad_port2;

int32_t fgfont;
int32_t bgfont;

void db_font_init(void)
{
	FntLoad(512 + 64, 256);

	fgfont = FntOpen(16, 16, 0, 0, 0, 256);
	bgfont = FntOpen(16, 16, 0, 0, 2, 256);
}

void db_init(int w, int h, int z, u_char r0, u_char g0, u_char b0)
{
	int oy = (h == 480) ? 0 : 240;

	ResetGraph(0);
	PadInitDirect((u_char *)&pad_port1, (u_char *)&pad_port2);
	PadStartCom();

	extern unsigned long __bss_end__;
	unsigned long bss_end = (unsigned long)&__bss_end__;
	unsigned long stack_start = 0x801f0000;

	printf("bss end %08x\n", &__bss_end__);
	printf("heap size %d\n", stack_start - bss_end);

	DW_DEBUG_ASSERT(bss_end < stack_start);

	InitHeap3((unsigned long *)bss_end, stack_start - bss_end);

	InitGeom();

	SetGeomOffset(w / 2, h / 2);
	SetGeomScreen(z);

	SetDefDrawEnv(&dw_draw[0], 0,  0, w, h);
	SetDefDispEnv(&dw_disp[0], 0, oy, w, h);
	SetDefDrawEnv(&dw_draw[1], 0, oy, w, h);
	SetDefDispEnv(&dw_disp[1], 0,  0, w, h);

	dw_draw[0].dtd = 0;
	dw_draw[1].dtd = 0;

	dw_draw[0].isbg = dw_draw[1].isbg = 1;
	setRGB0(&dw_draw[0], r0, g0, b0);
	setRGB0(&dw_draw[1], r0, g0, b0);
}

void db_swap(u_long *ot, int font_enabled)
{
	id = id? 0 : 1;
	if (dw_draw[id].dfe) {
		DrawSync(0);
		VSync(0);
	} else {
		VSync(0);
		ResetGraph(1);
	}
	PutDrawEnv(&dw_draw[id]);
	PutDispEnv(&dw_disp[id]);
	DrawOTag(ot);

	if (font_enabled) {
		FntFlush(-1);
	}
}

/* void db_set_matrix(MATRIX *m) */
/* { */
/* 	m->m[0][0] = m->m[0][0]*dw_disp[0].disp.w/320; */
/* 	m->m[0][1] = m->m[0][1]*dw_disp[0].disp.w/320; */
/* 	m->m[0][2] = m->m[0][2]*dw_disp[0].disp.w/320; */

/* 	m->m[1][0] = m->m[1][0]*dw_disp[0].disp.h/240; */
/* 	m->m[1][1] = m->m[1][1]*dw_disp[0].disp.h/240; */
/* 	m->m[1][2] = m->m[1][2]*dw_disp[0].disp.h/240; */
/* } */
