/* See LICENSE file for copyright and license details. */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#include <sys/types.h>

#include <libgte.h>
#include <libgpu.h>

#include "alloc.h"
#include "mtn.h"
#include "skeleton.h"
#include "utils.h"

#define DW_MTN_NUM_AXIS			9

#define DW_MTN_OPCODE_KEYFRAME		0x0000
#define DW_MTN_OPCODE_LOOP_START	0x1000
#define DW_MTN_OPCODE_LOOP_END		0x2000
#define DW_MTN_OPCODE_TEXTURE		0x3000
#define DW_MTN_OPCODE_SOUND		0x4000

typedef struct {
	int16_t value;
	int16_t scale;
	int16_t accum;
	uint16_t num_frames;
} dw_axis_momentum;

/* typedef struct dw_axis_momentum dw_axis_momentum; */

typedef struct {
	dw_axis_momentum axis[DW_MTN_NUM_AXIS];
} dw_node_momentum;

typedef struct {
	uint16_t num_frames;
	const int16_t initial_pose[0];
} dw_mtn_header;

struct dw_mtn {
	uint8_t *mtn_data;
	dw_node_momentum *momentum;
	uint16_t frame;
	uint16_t num_frames;
	uint16_t loop_frame;
	uint16_t loop_count;
	uint16_t last_keyframe;
	const uint16_t *cur_instr;
	const uint16_t *loop_instr;
	const int16_t *initial_pose;
	uint8_t num_nodes;
	RECT tim_prect;
	bool has_scale;
};

static int32_t momentum_get(dw_axis_momentum *m)
{
	int32_t v;

	if (m->scale) {
		v =  m->accum + ((m->value * m->num_frames) / m->scale);
	} else {
		v = 0;
	}

	return v;
}

static int32_t momentum_get_diff(dw_axis_momentum *m)
{
	int32_t cur;
	int32_t prev;
	int32_t v;

	if (m->scale && m->num_frames) {
		cur =  ((m->value * m->num_frames) / m->scale);
		prev =  ((m->value * (m->num_frames - 1)) / m->scale);
		v = cur - prev;
	} else {
		v = 0;
	}

	return v;
}

static void momentum_set(dw_axis_momentum *m, int16_t value, int16_t scale)
{
	m->accum = momentum_get(m);
	m->value = value;
	m->scale = scale;
	m->num_frames = 0;
}

static void dw_mtn_clear_momentum(dw_mtn *mtn)
{
	memset(mtn->momentum, 0, mtn->num_nodes * sizeof(*mtn->momentum));
}

dw_mtn *dw_mtn_init(uint8_t *mtn_data, uint8_t num_nodes, RECT *rect)
{
	dw_mtn *mtn;

	DW_DEBUG_ASSERT(num_nodes > 0);

	mtn = xcalloc(1, sizeof(*mtn));
	mtn->mtn_data = mtn_data;
	mtn->num_nodes = num_nodes;
	mtn->tim_prect = *rect;

	mtn->momentum = xcalloc(num_nodes, sizeof(*mtn->momentum));

	dw_mtn_play(mtn, 0);

	return mtn;
}

void dw_mtn_deinit(dw_mtn *mtn)
{
	xfree(mtn->mtn_data);
	xfree(mtn->momentum);
	xfree(mtn);
}

uint8_t dw_mtn_get_num_anim(const dw_mtn *mtn)
{
	return *((uint32_t *)mtn->mtn_data) / sizeof(uint32_t);
}

uint16_t dw_mtn_get_frame(const dw_mtn *mtn)
{
	return mtn->frame;
}

bool dw_mtn_anim_ended(const dw_mtn *mtn)
{
	return mtn->frame > mtn->num_frames;
}

void dw_mtn_get_root_motion(const dw_mtn *mtn, VECTOR *v)
{
	dw_node_momentum *m;

	m = &mtn->momentum[0];
	v->vx = momentum_get_diff(&m->axis[6]);
	v->vy = momentum_get_diff(&m->axis[7]);
	v->vz = momentum_get_diff(&m->axis[8]);
}

void
dw_mtn_play(dw_mtn *mtn, uint8_t id)
{
	const dw_mtn_header *mtn_header;
	const uint8_t *mtn_data;
	const uint16_t *instr;
	uint32_t offset;
	bool has_scale;
	uint16_t num_frames;

	mtn_data = mtn->mtn_data;
	DW_DEBUG_ASSERT(id < (*((uint32_t *)mtn_data) / sizeof(uint32_t)));

	offset = ((uint32_t *)mtn_data)[id];
	mtn_header = (const dw_mtn_header *)&mtn_data[offset];

	has_scale = mtn_header->num_frames & 0x8000;
	num_frames = mtn_header->num_frames & 0x7fff;

	if (has_scale) {
		offset = (mtn->num_nodes - 1) * DW_MTN_NUM_AXIS;
	} else {
		offset = (mtn->num_nodes - 1) * (DW_MTN_NUM_AXIS - 3);
	}

	instr = (const uint16_t *)&mtn_header->initial_pose[offset];

	mtn->frame = 1;
	mtn->num_frames = num_frames;
	mtn->cur_instr = instr;
	mtn->loop_instr = instr;
	mtn->initial_pose = &mtn_header->initial_pose[0];
	mtn->has_scale = has_scale;
	dw_mtn_clear_momentum(mtn);
}

void
dw_mtn_run(dw_mtn *mtn)
{
	const uint16_t *instr = mtn->cur_instr;
	dw_node_momentum *m;
	uint16_t opcode;
	uint16_t frame;
	uint16_t axis;
	int16_t value;
	int16_t scale;
	uint8_t node;
	RECT rect;
	int x;
	int y;
	int i;
	int j;

	while (*instr != 0x0000) {
		opcode = *instr & 0xf000;
		frame = *instr & 0x0fff;

		if ((opcode != DW_MTN_OPCODE_LOOP_START) &&
		    (frame != mtn->frame)) {
			break;
		}

		switch (opcode) {
		case DW_MTN_OPCODE_KEYFRAME: // keyframe
			/* printf("keyframe %d\n", *instr & 0xfff); */
			++instr; // inst

			if (frame == 1) {
				dw_mtn_clear_momentum(mtn);
			}

			while ((*instr & 0x8000)) {
				/* printf("process keyframe inst\n"); */
				axis = (*instr & 0x7fc0) >> 6;
				node = *instr & 0x3f;
				DW_DEBUG_ASSERT(node < mtn->num_nodes);
				m = &mtn->momentum[node];
				++instr; // keyframe_inst
				scale = *instr;
				++instr; // scale
				for (i = DW_MTN_NUM_AXIS - 1; i >= 0; --i) {
					/* printf("process axis %d\n", i); */
					if (axis & (1 << i)) {
						value = *instr;
						++instr;
					} else {
						value = m->axis[8 - i].value;
					}

					momentum_set(&m->axis[8 - i], value,
						     scale);
				}
			}

			break;
		case DW_MTN_OPCODE_LOOP_START: // loop start (255, 0 = endless)
			/* printf("loop start %d\n", *instr & 0xff); */
			mtn->loop_instr = instr;
			mtn->loop_count = *instr & 0xff;
			++instr; // inst
			break;
		case DW_MTN_OPCODE_LOOP_END: // loop end
			/* printf("loop end %d\n", *instr & 0xfff); */
			++instr; // inst

			mtn->frame = *instr; // newTime
			++instr;

			if (!mtn->loop_count || (mtn->loop_count == 255)) {
				/* Endless loop */
				instr = mtn->loop_instr;
				/* printf("endless loop to %d\n", mtn->frame); */
			} else {
				--mtn->loop_count;

				if (mtn->loop_count) {
					instr = mtn->loop_instr;
					/* printf("loop to %d %d\n", */
					/*        mtn->frame, mtn->loop_count); */
				}
			}
			// ++instr; // newTime
			break;
		case DW_MTN_OPCODE_TEXTURE: // texture
			/* printf("texture %d\n", *instr & 0xfff); */
			++instr; // inst

			rect = mtn->tim_prect;
			x = rect.x;
			y = rect.y;

			rect.y += ((const uint8_t *)instr)[0];
			rect.x += ((const uint8_t *)instr)[1];
			++instr;

			rect.h = ((const uint8_t *)instr)[0];
			rect.w = ((const uint8_t *)instr)[1];
			++instr;

			y += ((const uint8_t *)instr)[0];
			x += ((const uint8_t *)instr)[1];
			++instr;

			MoveImage(&rect, x, y);
			break;
		case DW_MTN_OPCODE_SOUND: // play sound
			/* printf("sound %d\n", *instr & 0xfff); */
			++instr; // inst
			++instr; // soundId, vabId
			break;
		}
	}

	mtn->cur_instr = instr;
	if (mtn->frame <= mtn->num_frames) {
		for (i = 0; i < mtn->num_nodes; ++i) {
			for (j = 0; j < DW_MTN_NUM_AXIS; ++j) {
				++mtn->momentum[i].axis[j].num_frames;
			}
		}
		++mtn->frame;
	}
}

void dw_mtn_apply(const dw_mtn *mtn, dw_skeleton *skeleton)
{
	dw_node *node;
	dw_node_momentum *m;
	uint8_t i;
	uint8_t j;

	DW_DEBUG_ASSERT(mtn->num_nodes <= skeleton->num_nodes);

	node = &skeleton->nodes[0];
	m = &mtn->momentum[0];

	node->scale.vx = ONE + momentum_get(&m->axis[0]);
	node->scale.vy = ONE + momentum_get(&m->axis[1]);
	node->scale.vz = ONE + momentum_get(&m->axis[2]);

	node->rotation.vx = momentum_get(&m->axis[3]);
	node->rotation.vy = momentum_get(&m->axis[4]);
	node->rotation.vz = momentum_get(&m->axis[5]);

	node->position = (VECTOR){ 0 };
	/* node->position.vx = momentum_get(&m->axis[6]); */
	/* node->position.vy = momentum_get(&m->axis[7]); */
	/* node->position.vz = momentum_get(&m->axis[8]); */

	if (mtn->has_scale) {
		for (i = 1, j = 0;
		     i < mtn->num_nodes;
		     ++i, j += DW_MTN_NUM_AXIS) {
			node = &skeleton->nodes[i];
			m = &mtn->momentum[i];

			node->scale.vx = (mtn->initial_pose[j + 0] +
					  momentum_get(&m->axis[0]));
			node->scale.vy = (mtn->initial_pose[j + 1] +
					  momentum_get(&m->axis[1]));
			node->scale.vz = (mtn->initial_pose[j + 2] +
					  momentum_get(&m->axis[2]));

			node->rotation.vx = (mtn->initial_pose[j + 3] +
					     momentum_get(&m->axis[3]));
			node->rotation.vy = (mtn->initial_pose[j + 4] +
					     momentum_get(&m->axis[4]));
			node->rotation.vz = (mtn->initial_pose[j + 5] +
					     momentum_get(&m->axis[5]));

			node->position.vx = (mtn->initial_pose[j + 6] +
					     momentum_get(&m->axis[6]));
			node->position.vy = (mtn->initial_pose[j + 7] +
					     momentum_get(&m->axis[7]));
			node->position.vz = (mtn->initial_pose[j + 8] +
					     momentum_get(&m->axis[8]));
		}
	} else {
		for (i = 1, j = 0;
		     i < mtn->num_nodes;
		     ++i, j += (DW_MTN_NUM_AXIS - 3)) {
			node = &skeleton->nodes[i];
			m = &mtn->momentum[i];

			node->scale.vx = (ONE + momentum_get(&m->axis[0]));
			node->scale.vy = (ONE + momentum_get(&m->axis[1]));
			node->scale.vz = (ONE + momentum_get(&m->axis[2]));

			node->rotation.vx = (mtn->initial_pose[j + 0] +
					     momentum_get(&m->axis[3]));
			node->rotation.vy = (mtn->initial_pose[j + 1] +
					     momentum_get(&m->axis[4]));
			node->rotation.vz = (mtn->initial_pose[j + 2] +
					     momentum_get(&m->axis[5]));

			node->position.vx = (mtn->initial_pose[j + 3] +
					     momentum_get(&m->axis[6]));
			node->position.vy = (mtn->initial_pose[j + 4] +
					     momentum_get(&m->axis[7]));
			node->position.vz = (mtn->initial_pose[j + 5] +
					     momentum_get(&m->axis[8]));
		}
	}
}
