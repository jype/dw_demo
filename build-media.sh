#!/bin/sh
# See LICENSE file for copyright and license details.

set -ex

usage() {
	printf 'usage: %s <path to extracted ISO>\n' "$0"
}

ISODIR="$1"
[ -z "$ISODIR" ] && { usage; exit 1; }

DIR="$(dirname "$(readlink -f "$0")")"
BUILDDIR="$DIR/build"
MEDIADIR="$BUILDDIR/media"

make -f "$DIR/tools/extract_mmd/Makefile"
make -f "$DIR/tools/tmd2pmd/Makefile"

rm -rf "$MEDIADIR"
mkdir -p "$MEDIADIR"

cp -a "$ISODIR/CHDAT" "$MEDIADIR"

for f in "$MEDIADIR/CHDAT/"*/*.MMD; do
	"$BUILDDIR/extract_mmd" "$f"
	"$BUILDDIR/tmd2pmd" "${f%.MMD}.TMD" "${f%.MMD}.PMD"
done

mkdir -p "$MEDIADIR/ETCDAT"
cp -a "$ISODIR/ETCDAT/"*.TIM "$MEDIADIR/ETCDAT"

cp -a "$ISODIR/MAP" "$MEDIADIR"
cp -a "$ISODIR/SOUND" "$MEDIADIR"
